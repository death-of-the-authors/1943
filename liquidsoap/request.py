#!/usr/bin/env python

from argparse import ArgumentParser
import telnetlib

parser = ArgumentParser(description='')
parser.add_argument('channel', help='request channel')
parser.add_argument('path', help='path')
parser.add_argument('--host', default="localhost", help='telnet host')
parser.add_argument('--port', type=int, default=1234, help='telnet port')
args = parser.parse_args()

tn = telnetlib.Telnet(args.host, args.port)

# tn.read_until("login: ")
# tn.write(user + "\n")
# if password:
#     tn.read_until("Password: ")
#     tn.write(password + "\n")

tn.write("{0}.push {1}\n".format(args.channel, args.path))
tn.write("quit\n")

print tn.read_all()
