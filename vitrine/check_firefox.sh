#!/bin/bash

echo Checking for Firefox...

while true
do

if ! ps ax | grep "[f]irefox/firefox"
then
echo Starting firefox...
/home/murtaugh/opt/firefox/firefox &

# Wait for it...
while ! ps ax | grep "[f]irefox/firefox"
do
	echo ...
	sleep 1
done


fi
sleep 5
done
