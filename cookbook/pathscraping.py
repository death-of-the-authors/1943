import sys, os, re, unittest

# def regressionTest():
#     path = os.getcwd()       
#     sys.path.append(path)    
#     files = os.listdir(path)

# get names of local files
# put this script in the directory where you want to scrape the filenames from
path = os.getcwd()
sys.path.append(path) 
files = os.listdir(path)
print files

# name link server as the weblink you want to compose your filemnames with
linkserver = "http://botopera.activearchives.org/Beatrix_Potter/drawings/1918_johnny_townmouse/"

# print linkserver & filename to text document, with introtext and each item on separate line
f = open('beatrix_index.txt', 'a')
for item in files:
	link = linkserver+item
	f.write("In 1918 I made this drawing for The Tale of Johnny Town-Mouse. " + link+"\n")

