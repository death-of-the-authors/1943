from __future__ import print_function
import sys
from thread import start_new_thread
from argparse import ArgumentParser
from random import randint


p = ArgumentParser(description="Channels output to a single output, 0 = stdout, n = routen given as parameter, -1 = random")
p.add_argument("--control", default=None, help="receive control signal from fifo/file (default: none)")
p.add_argument("--random", default=-1, type=int, help="value to use to route randomly, default: -1")
p.add_argument("--start", default=0, type=int, help="initial routing value, default: 0 (stdout)")
p.add_argument("--verbose", default=False, action="store_true", help="print messages to stderr")
p.add_argument("route", nargs="*", default=[], help="files to route to")

args = p.parse_args()
route = args.start

def listen (r):
	global route
	with open(r) as f:
		if args.verbose: print ("route: listening for control signal...", file=sys.stderr)
		while True:
			line = f.readline()
			if line == '':
				break
			route = int(line.rstrip())
			if args.verbose:
				sys.stderr.write("route: set to {0}\n".format(route))
				sys.stderr.flush()

# listen for control signals
if args.control:
	start_new_thread(listen, (args.control, ))

# open routes
routes = [sys.stdout]
for r in args.route:
	f = open(r, "w")
	routes.append(f)

useroute = route
while True:
	line = sys.stdin.readline()
	if line == '':
		break
	line = line.decode("utf-8")
	if route == args.random:
		useroute = randint(0, len(routes)-1)
	else:
		useroute = route
	routes[useroute].write(line.encode("utf-8"))
	routes[useroute].flush()

# cleanup
for r in routes[1:]:
	r.close()

