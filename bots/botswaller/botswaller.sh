#!/bin/bash
# ensure fifo
if [ ! -e msg.fifo ]; then mkfifo msg.fifo; fi
if [ ! -e delay.fifo ]; then mkfifo delay.fifo; fi
if [ ! -e route_control.fifo ]; then mkfifo route_control.fifo; fi
if [ ! -e route_selector.fifo ]; then mkfifo route_selector.fifo; fi
if [ ! -e router1_control.fifo ]; then mkfifo router1_control.fifo; fi
if [ ! -e router2_control.fifo ]; then mkfifo router2_control.fifo; fi
if [ ! -e route1.fifo ]; then mkfifo route1.fifo; fi
if [ ! -e route1a.fifo ]; then mkfifo route1a.fifo; fi
if [ ! -e route1b.fifo ]; then mkfifo route1b.fifo; fi
if [ ! -e route2.fifo ]; then mkfifo route2.fifo; fi
if [ ! -e collect.fifo ]; then mkfifo collect.fifo; fi

# start the pipeline
function route0 ()
{
cat msg.fifo | \
python ../ircpipebot.py --nickname botswaller | \
tee route_selector.fifo delay.fifo | \
python -u ../delay.py --delay 0.25 | \
python -u ../router.py --start 1 --control router1_control.fifo route1.fifo route1.fifo route2.fifo
}

function routeselector ()
{
cat route_selector.fifo | \
python -u ../routeselector.py | \
cat > route_control.fifo
}

function routecontrol () 
{
cat route_control.fifo | tee router1_control.fifo router2_control.fifo
}

function route1()
{
cat route1.fifo | \
python -u ../ircfilter.py --block beatrixbotter --block botswaller --block nickolatesla --block rachmanibot --block botopera_roland  --message | \
python -u ../router.py --start 1 --control router2_control.fifo route1a.fifo route1b.fifo /dev/null
}

function route1a()
{
cat route1a.fifo | \
python -u ../whooshbot.py --index ~/bots/bots.wikipedia fats.wikipedia.txt | \
cat > collect.fifo
}

function route1b()
{
cat route1b.fifo | \
python -u ../whooshbot.py --index ~/bots/bots.works fats.works.txt | \
cat > collect.fifo
}

function route1c()
{
cat collect.fifo | \
python -u ../delay.py --receive delay.fifo --delay 0.0 | \
cat > msg.fifo
}

function route2()
{
cat route2.fifo | \
python -u srtsinger.py | \
cat > msg.fifo
}

routecontrol &
routeselector &
route1 &
route1a &
route1b &
route1c &
route2 &
route0

# cleanup
# rm *.fifo