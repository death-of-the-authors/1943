from srt import aasrtparse
import irc.bot
from time import sleep
from random import random

titles = u"""
How you have by himself You are not fit (The Tale of Peter Rabbit)
Ashamed of his badness You manage to squeeze (The Tale of Benjamin Bunny)
You do when She came to mind if I (The Tale of Squirrel Nutkin)
Where do you eat my dinner quite comfortable? (The Tale of Tom Kitten)
Starts I feel bite you walking obediently (The Tale of Mrs. Tiggy-Winkle)
I don't want, ever so many as you (The Tale of Jemima Puddle-Duck)
I fear that he turned round, you nasty old toad (The Tale of Mrs. Tittlemouse)
Because they cannot spread it ever so many while we wait (The Tale of Timmy Tiptoes)
They could make two red lobsters to save him  (The Tale of Two Bad Mice)
Three kittens to keep them the air I tell you  (The Tale of Tom Kitten)
For three days neither for yellow indeed they DO the tropics have (The Tailor Of Glouchester)
No one hiding are you going, "They let you" (The Pie and the Patty-Pan) 
You bold bad spider, him go into they DO eat (The Tale of Mrs. Tittlemouse)
Of us could of us could not too heavy that I need (The Tale of Squirrel Nutkin)        
You bold bad spider, him go into they DO eat (The Tale of Mrs. Tittlemouse)
Of us could of us could not too heavy that I need (The Tale of Squirrel Nutkin)
""".strip().splitlines()

class MyBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "join"
        self.title_index = -1

    def on_privmsg(self, c, e):
        pass

    def on_pubmsg(self, c, e):
        # e.target == "#botopera"
        # BEGIN
        print e.source
        if "botswallerbot" in e.source.lower():
            if (self.title_index == -1):
                self.title_index = 0
                return
            if (self.title_index >= 0 and self.title_index < len(titles)):
                sleep(1+random()*1)
                c.privmsg(self.channel, "    "+titles[self.title_index].strip())
                self.title_index += 1

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='I am a bot!')
    parser.add_argument('--server', default='botopera.lan', help='server hostname (default: irc.freenode.net)')
    parser.add_argument('--port', default=6667, type=int, help='server port (default: 6667)')
    parser.add_argument('--channel', default='#botopera', help='channel to join (default: #botopera)')
    parser.add_argument('--nickname', default='beatrixBOTterBot', help='bot nickname (default: beatrixBOTTER)')

    args = parser.parse_args()
    bot = MyBot(args.channel, args.nickname, args.server, args.port)
    bot.start()

