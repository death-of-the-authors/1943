00:39.289 -->
When I'm with you I feel so small
00:41.548 -->
Right in my shell I want to crawl
00:43.767 -->
I wonder why I had to fall for you

00:48.002 -->
Why do you make my life so tough?
00:49.975 -->
Seems I can't love you half enough
00:52.441 -->
You're so heavenly, much too much for me

00:56.679 -->
I wish that I were twins, you great big babykins
01:00.914 -->
So I could love you twice as much as I do
01:04.828 -->
I'd have four loving arms to embrace you
01:09.064 -->
Four eyes to idolize you each time I face you

01:13.259 -->
With two hearts twice as true what couldn't four lips do
01:17.453 -->
When four ears hear you saying, "I'm yours"
01:21.606 -->
You great big babykins, I wish that I were twins
01:25.843 -->
So I could love you twice as much as I do

02:28.945 -->
You great big babykins, I wish that I were twins
02:32.615 -->
So I could love you twice as much as I do
