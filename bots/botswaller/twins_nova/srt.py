import re


def srtparse (src):
    """ parse srt src (string), returns list of dicts with start, end, timecode, content """
    spat = re.compile(r"^(?:\d+\n)?((?:\d\d:)?\d\d:\d\d(?:[\.,]\d\d\d)? *--> *(?:(?:\d\d:)?\d\d:\d\d(?:[\.,]\d\d\d)?)?)$", re.M)
    tcpat = re.compile(r"(?:(\d\d):)?(\d\d):(\d\d)(?:[\.,](\d\d\d))? *--> *(?:(?:(\d\d):)?(\d\d):(\d\d)(?:[\.,](\d\d\d))?)?")

    # spat = re.compile(r"^(?:\d+\n)?(\d\d:\d\d:\d\d(?:[,\.]\d\d\d)? --> \d\d:\d\d:\d\d(?:[,\.]\d\d\d)?)$", re.M)
    # tcpat = re.compile(r"(\d\d):(\d\d):(\d\d)(?:[,\.](\d\d\d))? --> (\d\d):(\d\d):(\d\d)(?:[,\.](\d\d\d))?")

    tt = spat.split(src)
    ret = []
    for i in range(1, len(tt), 2):
        timecode = tt[i]
        content = tt[i+1]
        tcs = tcpat.match(timecode).groups()
        if tcs[3] == None:
            start = (int(tcs[0])*3600) + (int(tcs[1])*60) + float(tcs[2])
        else:
            start = (int(tcs[0])*3600) + (int(tcs[1])*60) + float(tcs[2]+"."+tcs[3])
        if tcs[4]:
            if tcs[7] == None:
                end = (int(tcs[4])*3600) + (int(tcs[5])*60) + float(tcs[6])
            else:
                end = (int(tcs[4])*3600) + (int(tcs[5])*60) + float(tcs[6]+"."+tcs[7])
        else:
            end = None
        ret.append({
            'start': start,
            'end': end,
            'timecode': timecode,
            'content': content.strip()
        })
    return ret


AASRT_URL_PAT = re.compile(r"^(https?:\/\/.+?)$")
AASRT_TC_PAT = re.compile(r"^(?:(\d\d):)?(\d\d):(\d\d)(?:[,\.](\d{1,3}))? *--> *(?:(?:(\d\d):)?(\d\d):(\d\d)(?:[,\.](\d{1,3}))?)?$")

def parse_tc (tc, ret):
    if tc != None:
        ret['start'] = (int(tc[0] or 0) * 3600) + (int(tc[1]) * 60) + int(tc[2])
        if tc[3] != None:
            ret['start'] = ret['start'] + float("0."+tc[3])

        if tc[5] != None:
            ret['end'] = (int(tc[4] or 0) * 3600) + (int(tc[5]) * 60) + int(tc[6]) 
            if tc[7] != None:
                ret['end'] = ret['end'] + float("0."+tc[7])
    return ret

def aasrt_iter(f, line=0):
    url = None
    tc = None
    content = None
    content_linestart = None
    prev = None
    curline = line

    for line in f:
        curline += 1
        m = AASRT_URL_PAT.match(line)
        if m:
            if content != None:
                yield parse_tc(tc, {'href': url, 'content': content, 'lineStart': content_linestart, 'lineEnd': curline-1})
            url = m.group(1)
            tc = None
            content_linestart=curline
            content = u''
            prev = 'URL'
        else:
            m = AASRT_TC_PAT.match(line)
            if m:
                if prev != 'URL':
                    # freestanding timecode
                    if content != None:
                        yield parse_tc(tc, {'href': url, 'content': content, 'lineStart': content_linestart, 'lineEnd': curline-1})
                    content_linestart=curline
                    content = u''
                tc = m.groups()
                prev = 'TC'
            else:
                # Append content
                if content == None:
                    content_linestart=curline
                    content = line
                else:
                    content += line
                prev = 'TEXT'

    if content != None:
        yield parse_tc(tc, {'href': url, 'content': content, 'lineStart': content_linestart, 'lineEnd': curline})

def aasrt_normalized_iter (f):
    prev = None
    cur = None
    for cur in aasrt_iter(f):
        if prev != None:
            # prev.href => cur.href
            if ('href' in prev) and (prev['href'] != None) and (('href' not in cur) or (cur['href'] == None)):
                cur['href'] = prev['href']
            # prev.end <= cur.start
            if ('end' not in prev) and \
                 ('start' in cur) and \
                 cur['href'] == prev['href']:

                prev['end'] = cur['start']

            yield prev
        prev = cur
    if cur != None:
        yield cur

def aasrtparse (src, normalize=True):
    if (type(src) == str or type(src) == unicode):
        src = [x+"\n" for x in src.splitlines()]
    if normalize:
        return list(aasrt_normalized_iter(src))
    else:
        return list(aasrt_iter(src))

    # spat = re.compile(r"^(?:(\d+|http:\/\/.+?)\n)?((?:\d\d:)?\d\d:\d\d(?:[,\.]\d{1,3})? *--> *(?:(?:\d\d:)?\d\d:\d\d(?:[,\.]\d{1,3})?)?)$", re.M)

def timecode(secs, style='srt'):
    opts = timecode.styles[style]
    h = int(secs/3600)
    secs -= h*3600
    m = int(secs/60)
    secs -= m*60
    s = int(secs)

    if h == 0 and opts['skipzerohours']:
        r = "{0:02d}:{1:02d}".format(m, s)
    else:
        r = "{0:02d}:{1:02d}:{2:02d}".format(h, m, s)

    if (secs - s > 0) or opts['alwaysfract']:
        r += opts['delim']+("{:0.3f}".format(secs-s)[2:])

    return r

timecode.styles = {
    'srt': {
        'delim': ',',
        'alwaysfract': True,
        'skipzerohours': False
    },
    'compactsrt': {
        'delim': ',',
        'alwaysfract': False,
        'skipzerohours': True
    },
    'html5': {
        'delim': '.',
        'alwaysfract': False,
        'skipzerohours': True
    }
}

if __name__ == "__main__":
    doc1 = """
01:02:03 --> 
01:02:04.5 -->
http://ia700304.us.archive.org/4/items/etiquette_0709_librivox/etiquette_07_post.ogg
http://ia700304.us.archive.org/4/items/etiquette_0709_librivox/etiquette_07_post.ogg
http://ftp.belnet.be/FOSDEM/2008/maintracks/FOSDEM2008-largescale.ogg
02:04 -->

[[Large scale]]
02:15 -->
02:17 -->
http://cloud.vti.be/openhouse/video/02-OH-BartVandeput_20120221_a3v6-640x360.ogv
    """.strip()

    for t in aasrtparse(doc1):
        print t
        continue
        print "---"
        if t['href']:
            print t['href']
        if 'start' in t:
            # print tc
            if 'end' in t:
                print timecode(t['start']), '-->', timecode(t['end'])
            else:
                print timecode(t['start']), '-->'
        print t['content']

    # from pprint import pprint
    # for t in tt:
    #     print t
