from srt import aasrtparse
import irc.bot
from time import sleep, time

titles = aasrtparse(u"""00:39.289 -->
When I'm with you I feel so small
00:41.548 -->
Right in my shell I want to crawl
00:43.767 -->
I wonder why I had to fall for you

00:48.002 -->
Why do you make my life so tough?
00:49.975 -->
Seems I can't love you half enough
00:52.441 -->
You're so heavenly, much too much for me

00:56.679 -->
I wish that I were twins, you great big babykins
01:00.914 -->
So I could love you twice as much as I do
01:04.828 -->
I'd have four loving arms to embrace you
01:09.064 -->
Four eyes to idolize you each time I face you

01:13.259 -->
With two hearts twice as true what couldn't four lips do
01:17.453 -->
When four ears hear you saying, "I'm yours"
01:21.606 -->
You great big babykins, I wish that I were twins
01:25.843 -->
So I could love you twice as much as I do

02:28.945 -->
You great big babykins, I wish that I were twins
02:32.615 -->
So I could love you twice as much as I do
""")

class MyBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "join"
        sleep(10)
        c.privmsg(self.channel, "ready trix, here we go... https://archive.org/download/FatsWaller-TheCollectionI/06%20I%20Wish%20I%20Were%20Twins%20%201934.ogg")
        sleep(5)
        start = time()
        ti = 0
        while True:
            sleep(.5)
            elapsed = time() - start
            if elapsed > titles[ti]['start']:
                c.privmsg(self.channel, titles[ti]['content'].strip())
                ti += 1
            if ti >= len(titles):
                return

    def on_privmsg(self, c, e):
        pass

    def on_pubmsg(self, c, e):
        # e.target == "#botopera"
        # BEGIN
        pass

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='I am a bot!')
    parser.add_argument('--server', default='botopera.lan', help='server hostname (default: irc.freenode.net)')
    parser.add_argument('--port', default=6667, type=int, help='server port (default: 6667)')
    parser.add_argument('--channel', default='#botopera', help='channel to join (default: #botopera)')
    parser.add_argument('--nickname', default='botsWALLerBOT', help='bot nickname (default: BOTSwaller)')

    args = parser.parse_args()
    bot = MyBot(args.channel, args.nickname, args.server, args.port)
    bot.start()

