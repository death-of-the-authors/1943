#!/bin/bash
# ensure fifo
if [ ! -e botswaller.fifo ]; then mkfifo botswaller.fifo; fi
# start the pipeline
cat botswaller.fifo | \
python ircpipebot.py --nickname botswaller | \
tee botswaller/delay.fifo | \
python -u ircfilter.py --block beatrixbotter --block botswaller --block nickolatesla --block rachmanibot --message | \
python -u delay.py --delay 0.25 | \
python -u whooshbot.py --index ~/bots/bots.index wikipedia/fats.firstperson | \
python -u delay.py --receive botswaller/delay.fifo --delay 4.0 | \
cat > botswaller.fifo
# cleanup
if [ -e botswaller.fifo ]; then rm botswaller.fifo; fi