#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import nltk, re, pprint
from time import sleep
from random import choice, shuffle, random
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.book import *
from pattern.en import parse, parsetree, wordnet, ngrams
import irc.bot

# path to texts
#beatrix = "01_beatrix.txt"
peter = "Beatrixbotter/behaviour/tales/1_peter_rabbit.txt"
benjamin = "Beatrixbotter/behaviour/tales/2_benjamin_bunny.txt"
flopsy = "Beatrixbotter/behaviour/tales/3_flopsy_bunnies.txt"
tod = "Beatrixbotter/behaviour/tales/4_mr_tod.txt"
tom = "Beatrixbotter/behaviour/tales/5_tom_kitten.txt"
samuel = "Beatrixbotter/behaviour/tales/6_roly_poly_pudding.txt"
pie = "Beatrixbotter/behaviour/tales/7_pie_and_patty_pan.txt"
ginger = "Beatrixbotter/behaviour/tales/8_ginger_and_pickles.txt"
moppet = "Beatrixbotter/behaviour/tales/9_miss_moppet.txt"
nutkin = "Beatrixbotter/behaviour/tales/10_squirrel_nutkin.txt"
timmy = "Beatrixbotter/behaviour/tales/11_timmy_tiptoes.txt"
tailor = "Beatrixbotter/behaviour/tales/12_tailor_gloucester.txt"
johnny = "Beatrixbotter/behaviour/tales/13_johnny_townmouse.txt"
mice = "Beatrixbotter/behaviour/tales/14_two_bad_mice.txt"
tittlemouse = "Beatrixbotter/behaviour/tales/15_mrs_tittlemouse.txt"
tiggy = "Beatrixbotter/behaviour/tales/16_tiggy_winkle.txt"
rabbit = "Beatrixbotter/behaviour/tales/17_fierce_bad_rabbit.txt"
jemima = "Beatrixbotter/behaviour/tales/18_jemima_puddleduck.txt"
jeremy = "Beatrixbotter/behaviour/tales/19_jeremy_fisher.txt"
robinson = "Beatrixbotter/behaviour/tales/20_little_pig_robinson.txt"
pigling = "Beatrixbotter/behaviour/tales/21_pigling_bland.txt"

# list of texts
texts = [peter, benjamin, flopsy, tod, tom, samuel, pie, ginger, moppet, nutkin, timmy, tailor, johnny, mice, tittlemouse, 
tiggy, rabbit, jemima, jeremy, robinson, pigling] 

# functions
# random choice of tale
def select(texts):
    item = choice(texts)
    #print "selected txt", item
    return item


# gets title from tale
def title(txt):
    txt = open(txt).read().decode("utf-8")
    name = txt.splitlines()[0]
    #print "name", name
    return name

# gets url from tale
def source(txt):
    txt = open(txt).read().decode("utf-8")
    url = txt.splitlines()[1]
    #print "url", url
    return url

# splits text into list of words
def tokenize(txt):
    txt = open(txt).read().decode("utf-8")
    basket = ' '.join(line.replace('\n', '') for line in txt)
    #turn into wordlist
    basket = word_tokenize(txt)
    #print "tokens", basket
    return basket

# splits text into list of sentences
def sentences(txt):
    txt = ''.join(open(txt).readlines())
    basket = re.split(r' *[\.\?!][\'"\)\]]* *', txt)
    #print "sentences", basket
    return basket

# Select sentences that contain keyword
def sentences_with_key(sentences, word):
    basket = []
    for sentence in sentences:
        wordlist = sentence.split(" ")
        if word in wordlist:
            sentence = sentence+'.'
            basket.append(sentence)
    print "sentences_with_key", basket
    return basket

def length(msg):
    # captures last line in chat
    # splits chat message in wordlist
    words = msg.split()
    # selects from chat message words between 2 and 10 characters
    basket = []
    for word in words: 
        if len(word) > 2 and len(word) < 12:
            basket.append(word)
            print "basket words message", basket
    # picks a random word
    w = choice(basket)
    print "length word", w
    w = w.upper()
    print "length word capitals", w
    # define length keyword
    length = len(w)
    print "lenght", length
    # picks a random tale
    txt = select(texts)
    # captures title of tale
    nametxt = title(txt)
    # captures url of tale
    url = source(txt)
    # transforms txt in wordlist
    tokens = tokenize(txt)
    # transforms txt in list of sentences
    phrases = sentences(txt)
    # turns list of tokenized words into nltk.Text
    fin = nltk.Text(tokens)
    # selecteer op lengte van woorden
    T = set(fin)
    basket = [x for x in T if len(x) == length]
    sorted_words = sorted(basket)
    # picks word of sorted_words
    pick = choice(sorted_words)
    print "picked word from tale with right lenght", pick
    # Selects sentences in which selected word appears
    selected_sentences = sentences_with_key(phrases, pick)
    print "length selected sentences with key", selected_sentences
    # Shuffles the selected sentences
    shuffle(selected_sentences)
    # Prints the results
    basket = []
    if len(selected_sentences) > 0:
        pick = pick.upper()
        message1 = "The word, " + w + ", counts " + str(length) + " characters. In " + nametxt + " I used the following words of " + str(length) + " characters each: " + ', '.join(sorted_words[:4]) 
        #print(message1)
        basket.append(message1)
        printsentence = selected_sentences[0]
        #print(printsentence)
        pick = pick.upper()
        message2 = "Take the word, " + pick + ", for example, I used it in this sentence: " + printsentence.replace(pick, pick.upper())
        #print(message2)
        basket.append(message2)
        #print(message3)
    return basket
           
	
#msg = e.arguments[0]
#msg = raw_input('Please enter a sentence : ')



# Actions bot

class MyBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "join"
        
    def on_privmsg(self, c, e):
        pass

    def say (self, c, msg):
        for line in msg.splitlines():
            c.privmsg(self.channel, line)
            sleep(random()*2)

    def on_pubmsg(self, c, e):
        # e.target, e.source, e.arguments, e.type
        # captures last line in chat
        print e.arguments
        msg = e.arguments[0]
        messages = length(msg)
        for message in messages:
            self.say(c, message)

# Launch bot
if __name__ == "__main__":
    import sys
    if len(sys.argv) != 4:
        print "Usage: beatrixbotter_length.py <server[:port]> <channel> <nickname>"
        sys.exit(1)
    s = sys.argv[1].split(":", 1)
    server = s[0]
    if len(s) == 2:
        try:
            port = int(s[1])
        except ValueError:
            print "Error: Erroneous port."
            sys.exit(1)
    else:
        port = 6667
    channel = sys.argv[2]
    nickname = sys.argv[3]
    bot = MyBot(channel, nickname, server, port)
    bot.start()