#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import nltk, re, pprint
from time import sleep
from pattern.en import tag
from random import choice, random
import pickle
import re
import irc.bot

## this script takes a sentence as input, and generates a new similar sentence
## to make it work you need to run first source.py in order to get a pickledump called 'source.p'


# # 1. get a sentence as input 
# sentence = raw_input('Please enter a sentence : ')
# # define punctuation
# q = re.compile('^[ ?]*[?][ ?]*$')
# e = re.compile('^[ !]*[!][ !]*$')
# if q.match(sentence):
#     punct = "?"
# elif e.match(sentence):
#     punct = "!"
# else:
#     punct = "."



# 2. tag input sentence

def tagger_only(words):
    inputtags = []
    # split sentence into list of words
    for word in words:
    # create list with tuple of word/tag
        analise = tag(word)
    #selects tuple from list
        tagged_token = analise[0]
    # selects tag from tuple and adds it to new list
        inputtags.append(tagged_token[1])
    return inputtags



## 3. compare list of inputtags to keys of the saved dictionary 'source.p' and get their value
# import tags dictionary
# def pickled():
#     tags = open('source.p', 'rb')           
#     tags_pkld = pickle.load(tags)
#     #print tags_pkld
#     tags.close()
#     return tags_pkld

# # look for value and add to list
collection = []
possible_words = []
mirror = []
tags_pkld = {}
tags = open('Beatrixbotter/behaviour/tales/source.p', 'rb')           
tags_pkld = pickle.load(tags)
#print "tagsplickled",tags_pkld
tags.close()

def value(inputtags):
    ret = []
    for searchtag in inputtags:
        possible_words = [key for key, value in tags_pkld.items() if value == searchtag]
        #print "possible_words", possible_words
        #print 'tags_pkld', tags_pkld
        ret.append(possible_words)
    return ret

def parody(collection):
    ret = []
    for col in collection:
        pick = choice(col)
        ret.append(pick)
    return ret





# Actions bot

class MyBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "join"
        
    def on_privmsg(self, c, e):
        pass

    def say (self, c, msg):
        for line in msg.splitlines():
            c.privmsg(self.channel, line)
            sleep(random()*2)

    def on_pubmsg(self, c, e):
        # e.target, e.source, e.arguments, e.type
        # captures last line in chat
        global tags_pkld
        print e.arguments
        msg = e.arguments[0]
        words = msg.split()
        inputtags = tagger_only(words)
        #print "inputtags", inputtags
        collection = value(inputtags)
        print "collection", collection
        mirror = parody(collection)
        first = mirror[0]
        print "mirror", mirror
        message = first.capitalize() + " "+ ' '.join(mirror[1:])
        #print message
        self.say(c, message)

# Launch bot
if __name__ == "__main__":
    import sys
    if len(sys.argv) != 4:
        print "Usage: beatrixbotter_length.py <server[:port]> <channel> <nickname>"
        sys.exit(1)
    s = sys.argv[1].split(":", 1)
    server = s[0]
    if len(s) == 2:
        try:
            port = int(s[1])
        except ValueError:
            print "Error: Erroneous port."
            sys.exit(1)
    else:
        port = 6667
    channel = sys.argv[2]
    nickname = sys.argv[3]
    bot = MyBot(channel, nickname, server, port)
    bot.start()