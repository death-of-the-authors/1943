import irc.bot
import re, subprocess, os
from thread import start_new_thread
from time import sleep


summon_pat = re.compile(r"^summon (\w+)$", re.I)
kill_pat = re.compile(r"^kill (\w+)$", re.I)
info_pat = re.compile(r"^host:?\s*info$", re.I)

bots = ["beatrixbotter/beatrixbotter.sh", "botswaller/botswaller.sh", "nickolatesla/nickolatesla.sh"]

class HostBot (irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.server = server
        self.port = port
        self.channel = channel
        self.procs = []
        start_new_thread(self.thread, ())


    def on_welcome(self, c, e):
        c.join(self.channel)
        print "join"
        for b in bots:
            dirpath, filename = os.path.split(b)
            base, ext = os.path.splitext(filename)
            abspath = os.path.abspath(dirpath)
            absfile = os.path.join(abspath, filename)
            args = [absfile]
            print absfile, abspath
            p = subprocess.Popen(args, cwd=abspath, close_fds=True)
            self.procs.append((base, p))
            sleep(1)

    def on_privmsg(self, c, e):
        pass

    def thread (self):
        print "thread starting..."
        np = 0
        while True:
            returncode = None
            if len(self.procs) != np:
                np = len(self.procs)
                print "{0} processes...".format(np)
            for name, p in self.procs:
                returncode = p.poll()
                if returncode is not None:
                    self.procs.remove((name, p))
                    break
            if returncode:
                self.privmsg(self.channel, "{0} ended ({1})".format(name, returncode))
                print "Process ended {0}".format(returncode)
            else:
                sleep(1)

    def on_pubmsg(self, c, e):
        # print "on_pubmsg"
        # e.target, e.source, e.arguments, e.type
        # print e.arguments
        return
        msg = e.arguments[0]
        m = summon_pat.search(msg)
        if m:
            botname = m.group(1)
            # if botname in self.bot_processes:
            #     c.privmsg(self.channel, "{0} is already running".format(botname))
            # else:
            c.privmsg(self.channel, "Summoning {0}".format(botname))
            args = ["python", "{0}.py".format(botname), "--server", self.server, "--port", str(self.port), "--channel", self.channel]
            p = subprocess.Popen(args, close_fds=True)
            self.procs.append((botname, p))
        m = kill_pat.search(msg)
        if m:
            botname = m.group(1)
            for name, p in self.procs:
                if name == botname:
                    p.kill()
        m = info_pat.search(msg)
        if m:
            if len(self.procs) > 0:
                for name, p in self.procs:
                    c.privmsg(self.channel, "{0} is running".format(name))
            else:
                c.privmsg(self.channel, "No bots are running")

if __name__ == "__main__":
    import sys, argparse

    parser = argparse.ArgumentParser(description='I am a bot!')
    parser.add_argument('--server', default='irc.freenode.net', help='server hostname (default: irc.freenode.net)')
    parser.add_argument('--port', default=6667, type=int, help='server port (default: 6667)')
    parser.add_argument('--channel', default='#botopera', help='channel to join (default: #botopera)')
    parser.add_argument('--nickname', default='botoperahost', help='bot nickname (default: botoperahost)')
    # parser.add_argument('--index', action='store_true', default=False, help='index albums')
    # parser.add_argument('--search', default='', help='search the index (test)')
    parser.add_argument('--daemon', action="store_true", default=False, help='run as daemon (default: False)')

    args = parser.parse_args()
    bot = HostBot(args.channel, args.nickname, args.server, args.port)
    if args.daemon:
        import daemon
        # log = open("dorisbot.log", "w+")
        # with daemon.DaemonContext(stderr=log):
        with daemon.DaemonContext():
            bot.start()
    else:
        bot.start()


