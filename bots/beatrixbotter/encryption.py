#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

# replace vowels by numbers
def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text
    
# Replacements
reps = {'a':'4', 'e':'3', 'i':'1', 'o':'0', 'u':'8'}

def encryption(msg):
    # captures last line in chat
    txt = replace_all(msg, reps)
    message = "Hey, if I had to write down what you're saying today, I would say: " + txt + "."
    return message

if __name__ == "__main__":
    import sys

    while True:
        line = sys.stdin.readline()
        if line == "":
            break
        line = line.rstrip().decode("utf-8")
        print (encryption(line).encode("utf-8"))


