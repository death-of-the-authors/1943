#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import nltk, re, pprint
from time import sleep
from random import choice, shuffle
from nltk.tokenize import sent_tokenize, word_tokenize
#from nltk import FreqDist
# from pattern.en import wordnet, ngrams
import os, sys


thispath = os.path.dirname(__file__)

peter = "1_peter_rabbit.txt"
benjamin = "2_benjamin_bunny.txt"
flopsy = "3_flopsy_bunnies.txt"
tod = "4_mr_tod.txt"
tom = "5_tom_kitten.txt"
samuel = "6_roly_poly_pudding.txt"
pie = "7_pie_and_patty_pan.txt"
ginger = "8_ginger_and_pickles.txt"
moppet = "9_miss_moppet.txt"
nutkin = "10_squirrel_nutkin.txt"
timmy = "11_timmy_tiptoes.txt"
tailor = "12_tailor_gloucester.txt"
johnny = "13_johnny_townmouse.txt"
mice = "14_two_bad_mice.txt"
tittlemouse = "15_mrs_tittlemouse.txt"
tiggy = "16_tiggy_winkle.txt"
rabbit = "17_fierce_bad_rabbit.txt"
jemima = "18_jemima_puddleduck.txt"
jeremy = "19_jeremy_fisher.txt"
robinson = "20_little_pig_robinson.txt"
pigling = "21_pigling_bland.txt"

# list of texts
texts = [peter, benjamin, flopsy, tod, tom, samuel, pie, ginger, moppet, nutkin, timmy, tailor, johnny, mice, tittlemouse, 
tiggy, rabbit, jemima, jeremy, robinson, pigling] 

texts    = [os.path.join(thispath, "tales", x) for x in texts]

# functions
# random choice of tale
def select(texts):
    txt = choice(texts)
    return txt

# gets title from tale
def title(txt):
    txt = open(txt).read().decode("utf-8")
    nametxt = txt.splitlines()[0]
    return nametxt

# gets url from tale
def source(txt):
    txt = open(txt).read().decode("utf-8")
    url = txt.splitlines()[1]
    return url

# splits text into list of words
def tokenize(txt):
    txt = open(txt).read().decode("utf-8")
    tokens = ' '.join(line.replace('\n', '') for line in txt)
    #turn into wordlist
    tokens = word_tokenize(txt)
    return tokens

# splits text into list of sentences
def sentences(txt):
    txt = ''.join(open(txt).readlines())
    phrases = re.split(r' *[\.\?!][\'"\)\]]* *', txt)
    return phrases

# Select sentences that contain keyword
def sentences_with_pick(sentences, pick):
    selected_sentences = []
    for sentence in sentences:
        wordlist = sentence.split(" ")
        if pick in wordlist:
            sentence = sentence+'.'
            selected_sentences.append(sentence)
    return selected_sentences

# replace vowels by numbers
def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text

# Replacements
reps = {'a':'4', 'e':'3', 'i':'1', 'o':'0', 'u':'8'}

def length(msg):
    # captures last line in chat
    # splits chat message in wordlist
    words = msg.split()
    # selects from chat message words between 2 and 10 characters
    selected = []
    for word in words: 
        if len(word) > 3 and len(word) < 10:
            selected.append(word)
            #print(selected)
    # picks a random word
    if len(selected) == 0:
        print ''
        return

    w = choice(selected)
    w = w.upper()
    #print(w)
    # define length keyword
    length = len(w)
    #print(length)
    # picks a random tale
    txt = select(texts)
    # captures title of tale
    nametxt = title(txt)
    # captures url of tale
    url = source(txt)
    # transforms txt in wordlist
    tokens = tokenize(txt)
    # transforms txt in list of sentences
    phrases = sentences(txt)
    # turns list of tokenized words into nltk.Text
    fin = nltk.Text(tokens)
    # selecteer op lengte van woorden
    T = set(fin)
    words = [x for x in T if len(x) == length]
    sorted_words = sorted(words)
    # picks word of sorted_words
    pick = choice(sorted_words)
    #print(pick)
    # Selects sentences in which selected word appears
    selected_sentences = sentences_with_pick(phrases, pick)
    # Shuffles the selected sentences
    shuffle(selected_sentences)
    # Prints the results
    if len(selected_sentences) > 0:
        pick = pick.upper()
        message1 = "The word, " + w + ", has " + str(length) + " characters. In " + nametxt + " I used the following words of " + str(length) + " characters each: " + ', '.join(sorted_words[:4]) 
        print(message1)
        printsentence = selected_sentences[0]
        #print(printsentence)
        pick = pick.upper()
        message2 = "Take the word, " + pick + ", for example, I used it in this sentence: " + printsentence.replace(pick, pick.upper())
        print(message2)
        # message3 = "Source: " + nametxt 
        # print(message3)        

#msg = e.arguments[0]
while True:
    msg = sys.stdin.readline()
    if msg == '':
        break
    length(msg)
