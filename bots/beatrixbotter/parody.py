#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pattern.en import tag
from random import choice
import pickle, re, sys, os


## this script takes a sentence as input, and generates a new similar sentence
## to make it work you need to run first source.py in order to get a pickledump called 'source.p'


# 0. Prepare things
import os
thisfolder = os.path.dirname(__file__)
sourcepath = os.path.join(thisfolder, "parody_source.pkl")

if not os.path.exists(sourcepath):
	scriptpath = os.path.join(thisfolder, "parody_source.py")
	os.system("python "+scriptpath)

tags = open(sourcepath, 'rb')           
tags_pkld = pickle.load(tags)
#print tags_pkld
tags.close()

def tagger_only(list):
	inputtags = []
	for word in words:
	# create list with tuple of word/tag
 		analise = tag(word)
 	#selects tuple from list
 		tagged_token = analise[0]
 	# selects tag from tuple and adds it to new list
		inputtags.append(tagged_token[1])
	return inputtags


while True:
	# 1. get a sentence as input 
	sentence = sys.stdin.readline()
	if sentence == '':
		break
	sentence = sentence.rstrip()

	# define punctuation
	q = re.compile('^[ ?]*[?][ ?]*$')
	e = re.compile('^[ !]*[!][ !]*$')
	if q.match(sentence):
		punct = "?"
	elif e.match(sentence):
		punct = "!"
	else:
		punct = "."

	# split sentence into list of words
	words = sentence.split()

	# 2. tag input sentence
	inputtags = tagger_only(words)
	# print(inputtags)
	#This is a new sentence


	## 3. compare list of inputtags to keys of the saved dictionary 'source.p' and get their value
	# import tags dictionary

	# # look for value and add to list
	collection = []
	possible_words = []
	for searchtag in inputtags:
		possible_words = [key for key, value in tags_pkld.items() if value == searchtag]
		collection.append(possible_words)

	parody = []
	for words in collection:
		if len(words) > 0:
			pick = choice(words)
			parody.append(pick)
	if len(parody) > 0:
		first = parody[0]
		print (first.capitalize() + " "+ ' '.join(parody[1:]) + punct).encode("utf-8")
	else:
		print ""



