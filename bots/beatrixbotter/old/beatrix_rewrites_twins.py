from random import choice
from pattern.search import Pattern, STRICT, search
from pattern.en import parsetree, wordnet, ngrams

def re_search(text, search_string, strict=False):
    tree = parsetree(text, lemmata=True)
    if strict:
        results = search(search_string, tree, STRICT)
    else:
        results = search(search_string, tree)
    return results


def search_out(text, search_string, strict=False):
    results = re_search(text, search_string, strict)
    output = []
    for match in results:
        sent = []
        for word in match:
            sent.append(word.string)
        output.append(" ".join(sent))
    return output

# Part of speech Fats Waller's song 'I wish I were twins'

strings1 = ['WRB PRP VBP','IN PRP','PRP VBP RB JJ','JJ IN PRP$ NN','PRP VBP TO VB', 'PRP VBP WRB','PRP VBD TO','VB IN PRP']

# WRB PRP VBP       When I'm              When/WRB I/PRP 'm/VBP 
# IN PRP            with you              with/IN you/PRP 
# PRP VBP RB JJ     I feel so small       I/PRP feel/VBP so/RB small/JJ 
# JJ IN PRP$ NN     Right in my shell     Right/NN in/IN my/PRP$ shell/NN 
# PRP VBP TO VB     I want to crawl       I/PRP want/VBP to/TO crawl/VB 
# PRP VBP WRB       I wonder why          I/PRP wonder/VBP why/WRB 
# PRP VBD TO        I had to              I/PRP had/VBD to/TO 
# VB IN PRP         fall for you          fall/VB for/IN you/PRP

strings2 = ['WRB VBP PRP', 'VBP PRP$ NN','RB JJ','VBZ PRP VBP','VB PRP','NN RB', 'PRP VBP RB JJ','RB RB JJ','IN PRP']

# WRB VBP PRP       Why do you            Why/WRB do/VBP you/PRP 
# VBP PRP$ NN       make my life          make/VBP my/PRP$ life/NN 
# RB JJ             so tough              so/RB tough/JJ ?/.
# VBZ PRP VBP       Seems I can't         Seems/VBZ I/PRP ca/MD n't/RB 
# VB PRP            love you              love/VB you/PRP 
# NN RB             half enough           half/NN enough/RB 
# PRP VBP RB JJ     You're so heavenly    You/PRP 're/VBP so/RB heavenly/JJ
# RB RB JJ          much too much         much/RB too/RB much/JJ 
# IN PRP            for me                for/IN me/PRP

strings3 = ['PRP VBP IN','PRP VBD NN','PRP JJ JJ NN','IN PRP MD','VB PRP','RB RB JJ','IN PRP VBP']

# PRP VBP IN        I wish that           I/PRP wish/VBP that/IN 
# PRP VBD NN        I were twins          I/PRP were/VBD twins/NNS 
# PRP JJ JJ NN      you great big babykins    you/PRP great/JJ big/JJ babykins/NNS 
# IN PRP MD         So I could            So/IN I/PRP could/MD 
# VB PRP            love you              love/VB you/PRP 
# RB RB JJ          twice as much         twice/RB as/RB much/JJ 
# IN PRP VBP        as I do               as/IN I/PRP do/VBP 

strings4 = ['PRP MD VB','CD JJ NNS','TO VB PRP','CD NNS','TO VB PRP','DT NN','PRP VBP PRP','IN CD NNS','RB IN JJ']

# PRP MD VB         I'd have              I/PRP 'd/MD have/VB 
# CD JJ NNS         four loving arms      four/CD loving/JJ arms/NNS 
# TO VB PRP         ro embrace you        to/TO embrace/VB you/PRP 
# CD NNS            Four eyes             Four/CD eyes/NNS 
# TO VB PRP        to idolize you        to/TO idolize/VBP you/PRP 
# DT NN             each time             each/DT time/NN 
# PRP VBP PRP       I face you            I/PRP face/VBP you/PRP
# IN CD NNS         with two hearts       With/IN two/CD hearts/NNS 
# RB IN JJ          twice as true         twice/RB as/IN true/JJ 

strings5 = ['RB PRP VB','DT NN VBP','RB CD NN','VBP PRP VBG','PRP VBP PRP']

# RB PRP VB         what we couldn't      what/WP could/MD n't/RB 
# DT NN VB          four lips do          four/CD lips/NNS do/VBP 
# RB CD NN          When four ears        When/WRB four/CD ears/NNS 
# VBP PRP VBG       hear you saying       hear/VBP you/PRP saying/VBG 
# PRP VBP PRP       I'm yours             I/PRP 'm/VBP yours/PRP 

strings6 = ['PRP JJ JJ NN','PRP VB IN','PRP VB NN','IN PRP MD','VB PRP','RB RB JJ','IN PRP VBP']

# PRP JJ JJ NNS     you great big babykins    You/PRP great/JJ big/JJ babykins/NNS 
# PRP VB IN         I wish that           I/PRP wish/VBP that/IN 
# PRP VB NN        I were twins          I/PRP were/VBD twins/NNS 
# IN PRP MD         So I could            So/IN I/PRP could/MD 
# VB PRP            love you              love/VB you/PRP 
# RB RB JJ          twice as much         twice/RB as/RB much/JJ 
# IN PRP VBP        as I do               as/IN I/PRP do/VBP 

strings = strings1 + strings2 + strings3 + strings4 + strings5 + strings6
# to read you need to add a 'smart string', by using decode --> decode early, encode late (at output)
text = open('1_peter_rabbit.txt').read().decode("utf-8")
# search patterns in all Beatrix' tales
for string in strings:
    pattern = search_out(text, string, strict = False)
    # print(pattern)
    pick = choice(pattern)
    print(pick)
