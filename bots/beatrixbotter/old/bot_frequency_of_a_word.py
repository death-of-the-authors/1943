
# this needs input of a unique word/common word/word from chat

from __future__ import division
import nltk, re, pprint
from random import choice, shuffle
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.book import *
from pattern.en import parse, parsetree, wordnet, ngrams
import irc.bot


# path to texts
#beatrix = "01_beatrix.txt"
peter = "1_peter_rabbit.txt"
benjamin = "2_benjamin_bunny.txt"
flopsy = "3_flopsy_bunnies.txt"
tod = "4_mr_tod.txt"
tom = "5_tom_kitten.txt"
samuel = "6_roly_poly_pudding.txt"
pie = "7_pie_and_patty_pan.txt"
ginger = "8_ginger_and_pickles.txt"
moppet = "9_miss_moppet.txt"
nutkin = "10_squirrel_nutkin.txt"
timmy = "11_timmy_tiptoes.txt"
tailor = "12_tailor_gloucester.txt"
johnny = "13_johnny_townmouse.txt"
mice = "14_two_bad_mice.txt"
tittlemouse = "15_mrs_tittlemouse.txt"
tiggy = "16_tiggy_winkle.txt"
rabbit = "17_fierce_bad_rabbit.txt"
jemima = "18_jemima_puddleduck.txt"
jeremy = "19_jeremy_fisher.txt"
robinson = "20_little_pig_robinson.txt"
pigling = "21_pigling_bland.txt"

# List of texts
texts = [peter, benjamin, flopsy, tod, tom, samuel, pie, ginger, moppet, nutkin, timmy, tailor, johnny, mice, tittlemouse, 
tiggy, rabbit, jemima, jeremy, robinson, pigling] 


# Functions
def select(texts):
  txt = choice(texts)
  return txt

def title(txt):
  txt = open(txt).read().decode("utf-8")
  nametxt = txt.splitlines()[0]
  return nametxt
  
def source(txt):
  txt = open(txt).read().decode("utf-8")
  url = txt.splitlines()[1]
  return url

def tokenize(txt):
  txt = open(txt).read().decode("utf-8")
  tokens = ' '.join(line.replace('\n', '') for line in txt)
  #turn into wordlist
  tokens = word_tokenize(txt)
  return tokens

def sentences(txt):
  txt = ''.join(open(txt).readlines())
  sentences = re.split(r' *[\.\?!][\'"\)\]]* *', txt)
  return sentences

def sentences_with_sel(sentences):
  selected_sentences = []
  for sentence in sentences:
    wordlist = sentence.split(" ")
    if sel in wordlist:
      sentence = sentence+'.'
      selected_sentences.append(sentence)
  return selected_sentences



# Actions bot
class MyBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "join"
        
    def on_privmsg(self, c, e):
        pass

    def on_pubmsg(self, c, e):
        # e.target, e.source, e.arguments, e.type
        # captures last line in chat
        print e.arguments
        msg = e.arguments[0]
        # splits chat message in wordlist
        words = msg.split()
        # selects from chat message words between 2 and 10 characters
        selected = []
        for word in words: 
          if len(word) > 2 and len(word) < 10:
              selected.append(word)
              print(selected)
              # picks a random word
              sel = choice(selected)
              print(sel)
        # picks a random tale
        txt = select(texts)
        # captures title of tale
        nametxt = title(txt)
        # captures url of tale
        url = source(txt)
        # transforms txt in wordlist
        tokens = tokenize(txt)
        # transforms txt in list of sentences
        sentences = sentences(txt)
        # turn list of tokenized words into nltk.Text
        fin = nltk.Text(tokens)
        # FreqDist
        fdist1 = FreqDist(fin)
        print(fdist1)
        # Counts the frequency of the selected word
        amount = fdist1[sel]
        print(amount)
        # Selects sentences in which selected word appears
        selected_sentences = sentences_with_sel(sentences)
        # Shuffles the selected sentences
        shuffle(selected_sentences)
        # Prints the results
        message = "In " + nametxt + " I used " + str(amount) + " times the word " + sel + ". I selected four examples for you: "
        for sentence in selected_sentences[:4]:
          message1 = selected_sentences[0]
          message2 = selected_sentences[1]
          message3 = selected_sentences[2]
          message4 = selected_sentences[3]
        print(message1)
        print(message2)
        print(message3)
        print(message4)
        message5 = "Source: " + url + "."
        c.privmsg(self.channel, message)
        c.privmsg(self.channel, message1)
        c.privmsg(self.channel, message2)
        c.privmsg(self.channel, message3)
        c.privmsg(self.channel, message4)
        c.privmsg(self.channel, message5)


# Launch bot
if __name__ == "__main__":
    import sys
    if len(sys.argv) != 4:
        print "Usage: 0_bot_frequency_of_a_word.py <server[:port]> <channel> <nickname>"
        sys.exit(1)
    s = sys.argv[1].split(":", 1)
    server = s[0]
    if len(s) == 2:
        try:
            port = int(s[1])
        except ValueError:
            print "Error: Erroneous port."
            sys.exit(1)
    else:
        port = 6667
    channel = sys.argv[2]
    nickname = sys.argv[3]
    bot = MyBot(channel, nickname, server, port)
    bot.start()