# this needs input of a unique word/common word/word from chat

from __future__ import division
import nltk, re, pprint
from random import choice, shuffle
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.book import *
from pattern.en import parse, parsetree, wordnet, ngrams
import irc.bot
from time import sleep
from random import random

# path to texts
#beatrix = "01_beatrix.txt"
peter = "1_peter_rabbit.txt"
benjamin = "2_benjamin_bunny.txt"
flopsy = "3_flopsy_bunnies.txt"
tod = "4_mr_tod.txt"
tom = "5_tom_kitten.txt"
samuel = "6_roly_poly_pudding.txt"
pie = "7_pie_and_patty_pan.txt"
ginger = "8_ginger_and_pickles.txt"
moppet = "9_miss_moppet.txt"
nutkin = "10_squirrel_nutkin.txt"
timmy = "11_timmy_tiptoes.txt"
tailor = "12_tailor_gloucester.txt"
johnny = "13_johnny_townmouse.txt"
mice = "14_two_bad_mice.txt"
tittlemouse = "15_mrs_tittlemouse.txt"
tiggy = "16_tiggy_winkle.txt"
rabbit = "17_fierce_bad_rabbit.txt"
jemima = "18_jemima_puddleduck.txt"
jeremy = "19_jeremy_fisher.txt"
robinson = "20_little_pig_robinson.txt"
pigling = "21_pigling_bland.txt"

# list of texts
texts = [peter, benjamin, flopsy, tod, tom, samuel, pie, ginger, moppet, nutkin, timmy, tailor, johnny, mice, tittlemouse, 
tiggy, rabbit, jemima, jeremy, robinson, pigling] 


# functions
def select(texts):
  txt = choice(texts)
  return txt

def title(txt):
  txt = open(txt).read().decode("utf-8")
  nametxt = txt.splitlines()[0]
  return nametxt
  
def source(txt):
  txt = open(txt).read().decode("utf-8")
  url = txt.splitlines()[1]
  return url

def tokenize(txt):
  txt = open(txt).read().decode("utf-8")
  tokens = ' '.join(line.replace('\n', '') for line in txt)
  #turn into wordlist
  tokens = word_tokenize(txt)
  return tokens

def sentences(txt):
  txt = ''.join(open(txt).readlines())
  phrases = re.split(r' *[\.\?!][\'"\)\]]* *', txt)
  return phrases

def sentences_with_pick(sentences, pick):
  selected_sentences = []
  for sentence in sentences:
    wordlist = sentence.split(" ")
    if pick in wordlist:
      sentence = sentence+'.'
      selected_sentences.append(sentence)
  return selected_sentences


# Actions bot

class MyBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "join"
        
    def on_privmsg(self, c, e):
        pass

    def say (self, c, msg):
        for line in msg.splitlines():
            c.privmsg(self.channel, line)
            sleep(random()*2)

    def on_pubmsg(self, c, e):
        # e.target, e.source, e.arguments, e.type
        # captures last line in chat
        print e.arguments
        msg = e.arguments[0]
        # splits chat message in wordlist
        words = msg.split()
        # selects from chat message words between 2 and 10 characters
        selected = []
        for word in words: 
          if len(word) > 2 and len(word) < 10:
              selected.append(word)
              print(selected)
              # picks a random word
              sel = choice(selected)
              sel = sel.upper()
              print(sel)
        # define length keyword
        length = len(sel)
        print(length)
        # picks a random tale
        txt = select(texts)
        # captures title of tale
        nametxt = title(txt)
        # captures url of tale
        url = source(txt)
        # transforms txt in wordlist
        tokens = tokenize(txt)
        # transforms txt in list of sentences
        phrases = sentences(txt)
        # turn list of tokenized words into nltk.Text
        fin = nltk.Text(tokens)
        # selecteer op lengte van woorden
        T = set(fin)
        words = [w for w in T if len(w) == length]
        sorted_words = sorted(words)
        # pick word of sorted_words
        pick = choice(sorted_words)
        print(pick)
        # Selects sentences in which selected word appears
        selected_sentences = sentences_with_pick(phrases, pick)
        # Shuffles the selected sentences
        shuffle(selected_sentences)
        # Prints the results
        if len(selected_sentences) > 0:
          sel = sel.upper()
          message1 = "The word, " + sel + ", counts " + str(length) + " characters. In " + nametxt + " I used the following words of " + str(length) + " characters each: " + ', '.join(sorted_words[:4]) 
          print(message1)
          printsentence = selected_sentences[0]
          print(printsentence)
          pick = pick.upper()
          message2 = "Take the word, " + pick + ", for example, I used it in this sentence: " + printsentence.replace(pick, pick.upper())
          print(message2)
          message3 = "Source: " + nametxt 
          print(message3)
          self.say(c, message1)
          self.say(c, message2)
          self.say(c, message3)


# Launch bot
if __name__ == "__main__":
    import sys
    if len(sys.argv) != 4:
        print "Usage: 0_bot_selection_length_words.py <server[:port]> <channel> <nickname>"
        sys.exit(1)
    s = sys.argv[1].split(":", 1)
    server = s[0]
    if len(s) == 2:
        try:
            port = int(s[1])
        except ValueError:
            print "Error: Erroneous port."
            sys.exit(1)
    else:
        port = 6667
    channel = sys.argv[2]
    nickname = sys.argv[3]
    bot = MyBot(channel, nickname, server, port)
    bot.start()

