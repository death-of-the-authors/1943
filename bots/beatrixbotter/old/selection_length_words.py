# counting unique & most common words + average
# counting collocations
# counting active and passive words 'ing' and 'ed'
# TODO: er is iets mis met output van functie def select(texts), kan die niet hergebruiken


from __future__ import division
import nltk, re, pprint
from random import choice, shuffle
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.book import *
from pattern.en import parse, parsetree, wordnet, ngrams


# path to texts
#beatrix = "01_beatrix.txt"
peter = "1_peter_rabbit.txt"
benjamin = "2_benjamin_bunny.txt"
flopsy = "3_flopsy_bunnies.txt"
tod = "4_mr_tod.txt"
tom = "5_tom_kitten.txt"
samuel = "6_roly_poly_pudding.txt"
pie = "7_pie_and_patty_pan.txt"
ginger = "8_ginger_and_pickles.txt"
moppet = "9_miss_moppet.txt"
nutkin = "10_squirrel_nutkin.txt"
timmy = "11_timmy_tiptoes.txt"
tailor = "12_tailor_gloucester.txt"
johnny = "13_johnny_townmouse.txt"
mice = "14_two_bad_mice.txt"
tittlemouse = "15_mrs_tittlemouse.txt"
tiggy = "16_tiggy_winkle.txt"
rabbit = "17_fierce_bad_rabbit.txt"
jemima = "18_jemima_puddleduck.txt"
jeremy = "19_jeremy_fisher.txt"
robinson = "20_little_pig_robinson.txt"
pigling = "21_pigling_bland.txt"

# list of texts
texts = [peter, benjamin, flopsy, tod, tom, samuel, pie, ginger, moppet, nutkin, timmy, tailor, johnny, mice, tittlemouse, 
tiggy, rabbit, jemima, jeremy, robinson, pigling] 


# functions
def select(texts):
  txt = choice(texts)
  return txt

def title(txt):
  txt = open(txt).read().decode("utf-8")
  nametxt = txt.splitlines()[0]
  return nametxt
  
def source(txt):
  txt = open(txt).read().decode("utf-8")
  url = txt.splitlines()[1]
  return url

def tokenize(txt):
  txt = open(txt).read().decode("utf-8")
  tokens = ' '.join(line.replace('\n', '') for line in txt)
  #turn into wordlist
  tokens = word_tokenize(txt)
  return tokens

def sentences(txt):
  txt = ''.join(open(txt).readlines())
  sentences = re.split(r' *[\.\?!][\'"\)\]]* *', txt)
  return sentences

def sentences_with_pick(sentences):
  selected_sentences = []
  for sentence in sentences:
    wordlist = sentence.split(" ")
    if pick in wordlist:
      sentence = sentence+'.'
      selected_sentences.append(sentence)
  return selected_sentences


# Commands

# select keyword
sel = "little"
sel = sel.upper()
# define length keyword
length = len(sel)
print(length)
# pick random tale
txt = select(texts)
# capture title tale
nametxt = title(txt)
print(nametxt)
# capture url tale
url = source(txt)
# turn txt into wordlist
tokens = tokenize(txt)
# turn text into list of sentences
sentences = sentences(txt)
	
 
# turn string into nltk.Text
fin = nltk.Text(tokens)

# selecteer op lengte van woorden
T = set(fin)
words = [w for w in T if len(w) == length]
sorted_words = sorted(words)

# pick word of sorted_words
pick = choice(sorted_words)
print(pick)

# Selects sentences in which picked word appears
selected_sentences = sentences_with_pick(sentences)

# Shuffles the selected sentences
shuffle(selected_sentences)

# Prints the results
pick = pick.upper()
message1 = "The word, " + sel + ", counts " + str(length) + " characters. In " + nametxt + " I used the following words of " + str(length) + " characters each: " + "\"" + ', '.join(sorted_words) + ".\""
print(message1)
printsentence = selected_sentences[0]
#print(printsentence)
message2 = "Take the word, " + pick + ", for example, I used it in this sentence: " + printsentence
print(message2)
message3 = "Source: " + url + "."
print(message3)

 



