# this needs input of a unique word/common word/word from chat

from __future__ import division
import nltk, re, pprint
from random import choice, shuffle
from nltk.tokenize import sent_tokenize, word_tokenize
import irc.bot


# Function
# replace vowels by numbers
def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text

# Replacements
reps = {'a':'4', 'e':'3', 'i':'1', 'o':'0', 'u':'8'}

# Actions bot
class MyBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "join"
        
    def on_privmsg(self, c, e):
        pass

    # Return Nickname
    def GetNick(data):
        nick = data.split('!')[0]
        nick = nick.replace(':', ' ')
        nick = nick.replace(' ', '')
        nick = nick.strip(' \t\n\r')
        return nick

    def on_pubmsg(self, c, e):
        # e.target, e.source, e.arguments, e.type
        # captures last line in chat
        print e.arguments
        msg = e.arguments[0]
        print(msg)
        # splits chat message in wordlist
        # words = msg.split()
        txt = replace_all(msg, reps)
        # print message
        #nick = GetNick(msg)
        message = "Hey, if I had to write down what you're saying today, I would say: " + txt + "."
        print(message)
        c.privmsg(self.channel, message)


# Launch bot
if __name__ == "__main__":
    import sys
    if len(sys.argv) != 4:
        print "Usage: 0_encryption.py <server[:port]> <channel> <nickname>"
        sys.exit(1)
    s = sys.argv[1].split(":", 1)
    server = s[0]
    if len(s) == 2:
        try:
            port = int(s[1])
        except ValueError:
            print "Error: Erroneous port."
            sys.exit(1)
    else:
        port = 6667
    channel = sys.argv[2]
    nickname = sys.argv[3]
    bot = MyBot(channel, nickname, server, port)
    bot.start()


 
# # our text the replacement will take place
# text = "I write a sentence."
 

# # bind the returned text of the method
# # to a variable and print it
# txt = replace_all(text, reps)
# message = "Fats, Today I would write your sentence like this: " + txt + "."
# print(message)    # it prints '|-|3ll0 3v3ryb0dy'
 
# # of course we can print the result
# # at once with:
# # print replace_all(my_text, reps)