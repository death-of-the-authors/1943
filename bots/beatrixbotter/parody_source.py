#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from pattern.en import tag
from nltk.tokenize import word_tokenize
import pickle, sys


## this script creates a dictionary with POS for each word of Beatrix Potters' tales
## the pickle file functions as a source for the script parody.py

# choose file with tales
print ("Computing parody_source.pkl...", file=sys.stderr)
import os
thisfolder = os.path.dirname(__file__)
txt =os.path.join(thisfolder, "tales", '01_beatrix.txt')

# Tokenize or Transform text into word list
def tokenize(txt):
  txt = open(txt).read().decode("utf-8")
  tokens = ' '.join(line.replace('\n', '') for line in txt)
  tokens = word_tokenize(txt)
  return tokens

words = tokenize(txt)

# create dictionary with tags
tags = {}
def postagger(list):
	for w in words:
	# create list with tuple of word/tag
 		analise = tag(w)
 	#selects tuple from list
 		tagged_token = analise[0]
 	# selects word & tag from tuple and adds it to new list
 		word = tagged_token[0]
 		N = tagged_token[1]
 		tags[word]= N
	return tags

postagger(words)


# save dictionary as a pickle-file
path = os.path.join(thisfolder, "parody_source.pkl")
pickle.dump(tags,open(path, "wb"))
print ("Done", file=sys.stderr)
