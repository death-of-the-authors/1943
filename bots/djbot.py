#!/usr/bin/env python

import irc.bot, re, liquidsoap
from Queue import Queue
from thread import start_new_thread


urlpat = re.compile(r"https?://\S+")

def extract_urls (text):
    return [m.rstrip(".,()!?") for m in urlpat.findall(text)]

def process_requests (queue):
    # print "Speak messages"
    while True:
        urls = queue.get(block=True)
        if len(urls) > 0:
            print "REQUEST", urls[-1]
            liquidsoap.request("music", urls[-1])

def chunks(text, n):
    """ Yield successive n-sized chunks from l.
    """
    lines = text.splitlines()

    for line in lines:
        for i in xrange(0, len(line), n):
            yield line[i:i+n]

class MyBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel
        self.requests_queue = Queue()
        self.process_requests_thread = start_new_thread(process_requests, (self.requests_queue,))

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "join"

    def on_privmsg(self, c, e):
        pass

    def on_pubmsg(self, c, e):
        # e.target == "#botopera"
        if e.target == self.channel and e.type == "pubmsg":
            speaker_nick, speaker_nick_addr = e.source.split("!", 1)
            # e.target, e.source, e.arguments, e.type
            # print e.arguments
            msg = e.arguments[0].decode("utf-8")
            urls = extract_urls(msg)
            if len(urls) > 0:
                self.requests_queue.put(urls)
                # print "REQUEST", urls[0]

    def say_public(self, text):
        "Print TEXT into public channel, for all to see."
        for c in chunks(text, 400):
            print "say_public", type(c), c
            self.connection.privmsg(self.channel, c)

if __name__ == "__main__":
    import argparse, daemon

    parser = argparse.ArgumentParser(description='I am a bot!')
    parser.add_argument('--server', default='irc.freenode.net', help='server hostname (default: irc.freenode.net)')
    parser.add_argument('--port', default=6667, type=int, help='server port (default: 6667)')
    parser.add_argument('--channel', default='#botopera', help='channel to join (default: #botopera)')
    parser.add_argument('--nickname', default='botoperabot', help='bot nickname (default: botoperabot)')
    parser.add_argument('--daemon', action="store_true", default=False, help='run as daemon (default: False)')

    args = parser.parse_args()
    bot = MyBot(args.channel, args.nickname, args.server, args.port)
    if args.daemon:
        # pip install python-daemon
        import daemon
        # log = open("dorisbot.log", "w+")
        # with daemon.DaemonContext(stderr=log):
        with daemon.DaemonContext():
            bot.start()
    else:
        bot.start()

