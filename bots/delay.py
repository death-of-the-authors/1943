#!/usr/bin/env python

from __future__ import print_function
from argparse import ArgumentParser
from time import sleep
from thread import start_new_thread
import sys, random


p = ArgumentParser()
p.add_argument("--receive", default=None, help="receive control signal from fifo/file (default: none)")
p.add_argument("--delay", type=float, default=1.0, help="delay time in seconds, default: 1.0")
p.add_argument("--random", type=float, default=0.0, help="random time in seconds, default: 0")
p.add_argument("--verbose", default=False, action="store_true", help="print messages to stderr")
args = p.parse_args()

delay_count = 0
delay_amount = 0.0

def reset_delay_amount ():
	global delay_amount
	delay_amount = args.delay
	if args.random:
		delay_amount += args.random * random.random()

def delay():
	if args.verbose: print ("delay", file=sys.stderr)
	global delay_count
	delay_count += 1

def listen (r):
	with open(r) as f:
		if args.verbose: print ("delay: listening for control signal...", file=sys.stderr)
		while True:
			line = f.readline()
			if line == '':
				break
			# DELAY!
			delay()

# listen for control signals
if args.receive:
	start_new_thread(listen, (args.receive, ))
	use_delay_count = True
else:
	use_delay_count = False

while True:
	line = sys.stdin.readline()
	if line == '':
		break
	line = line.decode("utf-8")

	if use_delay_count:
		while delay_count > 0:
			sleep(args.delay)
			delay_count -= 1
	else:
		sleep(args.delay)
	reset_delay_amount()

	sys.stdout.write(line.encode("utf-8"))
	sys.stdout.flush()

