from argparse import ArgumentParser
import re, sys

p = ArgumentParser()
p.add_argument("subs", default=None, help="file containing substitutions, one per line, split by one or more tabs")
p.add_argument("--case", default=False, action="store_true", help="case sensitive")
p.add_argument("--no-boundary", default=False, action="store_true", help="don't pad match with word substitutions")
args = p.parse_args()

# Read the substitution patterns
with open(args.subs) as f:
    lines = [x.strip() for x in f.readlines() if x.strip() and not x.startswith("#")]
    subs = [[p.strip() for p in re.split(r"\t+", x.decode("utf-8"), maxsplit=1)] for x in lines]

for line in sys.stdin:
    line = line.decode("utf-8")
    for search, replace in subs:
    	if not args.no_boundary:
    		search = r"\b{0}\b".format(search)
        if args.case:
            line = re.sub(search, replace, line)
        else:
            line = re.sub(search, replace, line, flags=re.I)
    sys.stdout.write(line.encode("utf-8"))

