import sys, re
from random import choice
from whooshutils import *


subs = [
	(ur'\b(THOMAS )?(WRIGHT )?("?FATS"? )?WALLER\b', u"WALLER"),
    (ur"\bwaller's\b", 	u"his"),
    (ur"\bwaller\b", 	u"he"),
    (ur"\bhe\b", 		u"I"),
    (ur"\bhis\b", 		u"my"),
    (ur"\bhim\b",		u"me")
]

def sub (msg):
    for search, replace in subs:
        msg = re.sub(search, replace, msg, flags=re.I)
    return msg

def respond (index, msg):
    search_results = index_search(index, msg)
    if search_results:
        return sub(choice(search_results)['text'])

if __name__ == "__main__":

    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("--index", default="~/bots/fatswaller.wikipedia.index")
    parser.add_argument("--train", default=None)
    args = parser.parse_args()

    if args.train:
        ix = get_or_create_index(os.path.expanduser(args.index))
        with open(args.train) as f:
            index_sentences(ix, f)
    else:
        ix = get_index(os.path.expanduser(args.index))
        while True:
        	line = sys.stdin.readline()
        	if line == '':
        		break
        	r = respond(ix, line)
        	if r:
        		print r.encode("utf-8")
        	else:
        		print ''

