import sys, re
from argparse import ArgumentParser
from pattern.en import parse, pprint

p = ArgumentParser()
p.add_argument("sentences")
# p.add_argument("--index", default=0, type=int)
args = p.parse_args()

with open(args.sentences) as f:
	sentences = [x.decode("utf-8") for x in f.readlines()]

def third_to_first_person (s):
	s = re.sub(ur"waller's", "His", s, flags=re.I)
	s = re.sub(ur'(THOMAS )?(WRIGHT )?("FATS" )?WALLER', "WALLER", s, flags=re.I)

	ps = parse(s)
	words = ps.split()
	out = []
	for s in words:
	 	for i, parts in enumerate(s):
	 		(word, partofspeech, chunk, pnp) = parts
	 		pp = (partofspeech, chunk, pnp)
	 		lword = word.lower()
	 		r = word
	 		if lword == "waller":
	 			if pp in (('NNP', 'B-NP', 'O'), ('NNP', 'I-NP', 'O')):
	 				r = "_I_ [{0} ('{1}','{2}','{3}')]".format(word, partofspeech, chunk, pnp)
	 			elif pp in (('VB','I-VP','O'),):
	 				r = "_me_ [{0} ('{1}','{2}','{3}')]".format(word, partofspeech, chunk, pnp)
	 			else:
	 				r = "[{0} ('{1}','{2}','{3}')]".format(word, partofspeech, chunk, pnp)
	 		elif lword == "he":
	 			if pp in (('PRP','B-NP','O'), ('PRP','I-NP','I-PNP'), ('PRP','I-NP','O')):	
	 				r = "_I_ [{0} ('{1}','{2}','{3}')]".format(word, partofspeech, chunk, pnp)
	 			else:
	 				r = "[{0} ('{1}','{2}','{3}')]".format(word, partofspeech, chunk, pnp)
	 		elif lword == "his":
	 			if pp in (('PRP$','B-NP','I-PNP'), ('PRP$','B-NP','O')):
	 				r = "_my_ [{0} ('{1}','{2}','{3}')]".format(word, partofspeech, chunk, pnp)
	 			else:
					r = "[{0} ('{1}','{2}','{3}')]".format(word, partofspeech, chunk, pnp)
	 		elif lword == "him":
	 			if pp in (('PRP','B-NP','O'), ):
	 				r = "_me_ [{0} ('{1}','{2}','{3}')]".format(word, partofspeech, chunk, pnp)
	 			else:
					r = "[{0} ('{1}','{2}','{3}')]".format(word, partofspeech, chunk, pnp)
			out.append(r)
	s2 = u" ".join(out)
	return s2

for s in sentences:
	print third_to_first_person(s).encode("utf-8")
