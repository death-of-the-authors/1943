from argparse import ArgumentParser
import json, sys


p = ArgumentParser()
p.add_argument("path", help="path to json file")
p.add_argument("--mediawiki-title", default=False, action="store_true", help="show mediawiki_title")
args = p.parse_args()

with open(args.path) as f:
	data = json.load(f)

if args.mediawiki_title:
	sys.stdout.write(data.get("mediawiki_title"))
