import json

path = "fats.json"

with open(path) as f:
	data = json.load(f)

for r in data['revisions']:
	print "."*40
	print r['content'][:200]
