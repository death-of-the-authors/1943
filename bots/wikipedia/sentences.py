from __future__ import print_function
import json, sys, re, subprocess


def strip_markdown_headers (text):
	return re.sub(ur"^#.+$", u"", text, flags=re.M)

def strip_markdown_lists (text):
	return re.sub(ur"^\-.+$", u"", text, flags=re.M)

def strip_markdown_footnotes (text):
	return re.sub(ur"^\[\^\d+\]:.*$", u"", text, flags=re.M)

def strip_markdown_footnote_refs (text):
	return re.sub(ur"\[\^\d+\]", u"", text)

def pandoc (text, ffrom="mediawiki", to="markdown"):
	args = ["pandoc", "--from", ffrom, "--to", to, "--no-wrap", "--atx-headers"]
	p = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
	(out, err) = p.communicate(text.encode("utf-8"))
	return out.decode("utf-8")

# templates = re.compile(r"\{\{.*?\}\}", re.DOTALL)
tables_pat = re.compile(ur"\{\|.*?\|\}", re.DOTALL)
def strip_mw_tables (text):
	text = tables_pat.sub("", text)
	return text

def strip_blank_lines (text):
	return re.sub(ur"^\s*$", u"", text, flags=re.M)

def clean_text (text):
	text = strip_mw_tables(text)
	text = pandoc(text, ffrom="mediawiki", to="markdown")
	text = strip_markdown_footnotes(text)
	text = strip_markdown_footnote_refs(text)
	text = strip_markdown_headers(text)
	text = strip_markdown_lists(text)
#	text = strip_blank_lines(text)
	text = pandoc(text, ffrom="markdown", to="plain")
	# strip Category: lines
	text = re.sub(ur"^Category:.*$", u"", text, flags=re.M)
	text = re.sub(ur"^Source:.*$", u"", text, flags=re.M | re.I)
	text = strip_blank_lines(text)
	return text

path = sys.argv[1]
with open(path) as f:
	# data = json.load(f)
	text = f.read().decode("utf-8")
# text = data['revisions'][0]['content']

text = clean_text(text)
# print text.encode("utf-8")


# from pattern.en import tree, parse
# t = tree(parse(text, tokenize=True, tags=False, chunks=False, relations=False, lemmata=False))
# for i, s in enumerate(t):
# 	print (u"{0}".format(s).encode("utf-8"))
# print ("{0} total sentences".format(len(t)), file=sys.stderr)

import nltk.data
sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
sentences = sent_detector.tokenize(text)
for i, s in enumerate(sentences):
	s = re.sub("[\n\r]", " ", s)
	# print (u"{0:04d} {1}".format(i+1, s).encode("utf-8"))
	print (u"{0}".format(s).encode("utf-8"))
print ("{0} total sentences".format(len(sentences)), file=sys.stderr)
