from argparse import ArgumentParser
import json, sys, re


p = ArgumentParser()
p.add_argument("infopath", help="path to info json file")
args = p.parse_args()

with open(args.infopath) as f:
	data = json.load(f)

i_subs = data["i_substitutions"]
my_subs = data["my_substitutions"]

i_pat = u"|".join([re.escape(x) for x in i_subs])
i_pat = re.compile(ur"\b({0})\b".format(i_pat), flags=re.I)

my_pat = u"|".join([re.escape(x) for x in my_subs])
my_pat = re.compile(ur"\b({0})\b".format(my_pat), flags=re.I)

for line in sys.stdin:
	text = line.strip()
	text = my_pat.sub("my", text)
	text = i_pat.sub("I", text)
	print text
