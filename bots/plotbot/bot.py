#!/usr/bin/env python2
import irc.bot
from datetime import datetime
from random import randint
import re
import subprocess
import time
import thread

MSG_FOUND_IMAGE = "Hey, thats an image, let me draw it for you"
MSG_IGNORED_IMAGE = "Hey, thats an image, but I'm already drawing"
MSG_FINISHED_IMAGE = "Hey, I finished drawing that image"

image_pattern = re.compile('http(s)?://.*\.(png|jpg|bmp|jpeg|gif)', re.I)
ext_pattern = re.compile('\.(png|jpg|bmp|jpeg|gif)$', re.I)

margin_pattern = re.compile('(-?\d+)\s*,\s*(-?\d+)\s*,\s*(-?\d+)\s*,\s*(-?\d+)')
state_pattern = re.compile('currently (.+)')
finished_pattern = re.compile('great news')

nick_plotbot = 'botopera_roland'

class PlotBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel
        self.pens = []
        self.margins = None
        self.image = None
        self.pen = None
        
    def get_pen(self, nick):
        if nick not in self.pens:
            self.pens.append(nick)
        
        return self.pens.index(nick) + 1

    def get_nick(self, source):
        if '!' in source:
            return source.split('!',1)[0]
        return source
    
    def on_welcome(self, c, e):
        c.join(self.channel,)
        print "Bot connected"
        self.send(c, 'what is the pagesize?')

    def on_privmsg(self, c, e):
        msg = e.arguments[0]
        
        print "Received msg: '{0}'".format(msg)
        
        if finished_pattern.search(msg) <> None:
            self.image = None
            self.send_public(c, MSG_FINISHED_IMAGE)
            
        elif margin_pattern.search(msg) <> None:
            print 'found margins'
            margin_match = margin_pattern.search(msg)
            self.margins = {
                'left': int(margin_match.group(1)),
                'bottom': int(margin_match.group(2)),
                'right': int(margin_match.group(3)),
                'top': int(margin_match.group(4))
            }
            print self.margins
            
        elif state_pattern.search(msg) <> None:
            state = state_pattern.search(msg).group(1)
            
            if state == 'waiting':
                print ('DRAW Image')
                self.draw_image(c)
            else:
                print ('drop Image')
                self.drop_image(c)
    
    def send(self, c, msg):
        max_length = 350
        
        if len(msg) <= max_length + 2:
            c.privmsg(nick_plotbot, msg)
        else:
            c.privmsg(nick_plotbot, msg[0:max_length])
            time.sleep(1)
            self.send(c, msg[max_length:])
    
    def send_public(self, c, msg):
        c.privmsg(self.channel, msg)
    
    def draw_image(self, c):
        print self.image
        if self.image <> None:
            extension_match = ext_pattern.search(self.image)
            print extension_match
            if (extension_match <> None):
                extension = extension_match.group(1)
                localName = 'drawings/latest'

                imgWidth = '2000'
                imgHeight = '2000'

                subprocess.call([
                    './hpgl-line-drawing.sh',
                    self.image,
                    localName,
                    extension,
                    imgWidth,
                    imgHeight
                ])

                handle = open('{0}.hpgl'.format(localName), 'r')
                hpgl = ''.join(handle.readlines()).replace('\n', '')

                width = self.margins['right'] - self.margins['left']
                height = self.margins['top'] - self.margins['bottom']
                
                scale = .9
                
                x = randint(2000, width - 5000) * -1
                y = randint(2000, height - 5000) * -1
                
                thread.start_new_thread(self.send, (c, '<<SC{0},{1},{2},{3};SP{4};FS1;VS10;PU;{5};PU>>'.format(x, width + x, y, y + height, self.pen, hpgl)))
    
    def drop_image(self, c):
        self.image = None
    
    def on_pubmsg(self, c, e):
        message = e.arguments[0]
        nick = self.get_nick(e.source)

        found_image = image_pattern.search(message)

        if found_image <> None and self.margins <> None:
            if self.image == None:
                self.image = found_image.group(0)
                self.pen = self.get_pen(nick)
                self.send_public(c, MSG_FOUND_IMAGE)
                self.send(c, 'what are you doing?')
            else:
                self.send_public(c, MSG_IGNORED_IMAGE)
                
            
            
if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description='drawbot')
    parser.add_argument('--host', default="localhost", help='host')
    parser.add_argument('--port', type=int, default=6667, help='port')
    parser.add_argument('channel', help='channel to join')
    args = parser.parse_args()
    
    if not args.channel.startswith("#"):
        args.channel = "#"+args.channel
    
    bot = PlotBot(args.channel, 'drawbot', args.host, args.port)
    bot.start()