from writer import Writer

# Exports the font object to an JSON file
class FontWriter (object):
    pointReplacementPatt = re.compile ("(\s+)(\-?\d+),\n\s+(\-?\d+)", flags=re.M)

    def __init__ (self, font = False):
        if font <> False:
            self.font = font

    def write (self, path):
        if self.font <> False:
            writeList = []

    for char in self.font.chars:
        #writeObject = {}
        #writeObject['key'] = self.font.chars[char].key
        #writeObject['width'] = self.font.chars[char].width
        #writeObject['height'] = self.font.chars[char].height
        #writeObject['lines'] = self.font.chars[char].lines
        #
        writeObject = dict (
            key = self.font.chars[char].key,
            width = self.font.chars[char].width,
            height = self.font.chars[char].height,
            lines = self.font.chars[char].lines,
            margins = self.font.chars[char].margins
        )
        
        writeList.append (writeObject)

    self.writer = Writer ({'name': self.font.name, 'chars': writeList})

    buff = self.pointReplacementPatt.sub ("\\1\\2,\\3", self.writer.build ())

    try:
        with open (path, 'w') as self.f:
            self.f.write (buff)
            self.f.close ()

    except IOError:
        return False

    return True 
                    