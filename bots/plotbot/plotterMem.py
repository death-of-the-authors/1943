def enlargeMemory (plotter):
    if plotter.type == "HP7550A":
        plotter._serial_port.write (chr(27) + '.T10978;1778;0;0;44:')
        plotter._send_query (chr(27) + '.L')
        plotter._serial_port.write (chr(27) + '.@10978:')
        plotter.buffer_size = int (plotter._buffer_space / 2)
    elif plotter.type == "HP7596A":
        plotter._serial_port.write (chr(27) + '.T20552;1024;0;0;0;1024:')
        plotter._send_query (chr(27) + '.L')
        plotter._serial_port.write (chr(27) + '.@20552:')
        plotter.buffer_size = int (plotter._buffer_space / 2)
    elif plotter.type == 0400 or plotter.type == 'GRX-400AG':
        plotter._serial_port.write (chr(27) + '.T14337;3072;0;0;0;1024:')
        plotter._send_query (chr(27) + '.L')
        plotter._serial_port.write (chr(27) + '.@14337:')
        plotter.buffer_size = int (plotter._buffer_space / 2)
    elif plotter.type == 3500 or plotter.type == 'DXY-3500':
        plotter._serial_port.write (chr(27) + '.T14337;3072;0;0;0;1024:')
        plotter._send_query (chr(27) + '.L')
        plotter._serial_port.write (chr(27) + '.@14337:')
        plotter.buffer_size = int (plotter._buffer_space / 2)

    return plotter.buffer_size