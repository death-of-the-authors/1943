#!/usr/bin/env python2
from random import randint
from sys import argv
from PIL import Image

inpath = argv[1]
outpath = argv[2]
max_width = int(argv[3])
max_height = int(argv[4])

im_in = Image.open(inpath)
if (im_in.format == 'GIF'):
    seek_image = Image.open(inpath)
    frames = 1
    while frames < 100:
        try:
            seek_image.seek(frames)
            frames += 1
        except EOFError:
            break; 

    frame = randint(0,frames-1) if frames > 1 else 0
    im_in.seek(frame)

width,height = im_in.size

scale = 1

if width > 2000:
    scale = 200.0 / width
elif height > 2000:
    scale = 2000.0 / height
    
if scale <> 1:
    im_in.resize((
        int(width * scale),
        int(height * scale),
    ))
    
    width,height = im_in.size

im_in = im_in.copy()

if width > max_width or height > max_height:
    x = randint(0, max(0, width - max_width))
    y = randint(0, max(0, height - max_height))
    box = (x, y, min(width, x + max_width), min(height, y + max_width))
    im_in = im_in.crop(box)

im_in.save(outpath)