#!/usr/bin/env python2
import irc.bot
from datetime import datetime
from random import randint
import re
import subprocess
import time
import thread
import serial

from plotterMem import enlargeMemory

from chiplotle.tools.plottertools._instantiate_plotter import _instantiate_plotter
from chiplotle.tools.serialtools import what_plotter_in_port

MSG_FOUND_IMAGE = "Hey, thats an image, let me draw it for you"
MSG_IGNORED_IMAGE = "Hey, thats an image, but I'm already drawing"
MSG_FINISHED_IMAGE = "Hey, I finished drawing that image"
MSG_FAILED_IMAGE = "Sorry, something broke in the process"

PLOTTER_SIGNATURE = '0400'
PLOTTER_PORT= '/dev/ttyUSB0'

image_pattern = re.compile('http(s)?://.*\.(png|jpg|bmp|jpeg|gif)', re.I)
ext_pattern = re.compile('\.(png|jpg|bmp|jpeg|gif)$', re.I)

margin_pattern = re.compile('(-?\d+)\s*,\s*(-?\d+)\s*,\s*(-?\d+)\s*,\s*(-?\d+)')
state_pattern = re.compile('currently (.+)')
finished_pattern = re.compile('great news')

class PlotBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel
        self.plotter = None
        self.pens = []
        self.margins = None
        self.image = None
        self.pen = None
        self.wait = 4
        
    def get_pen(self, nick):
        if nick not in self.pens:
            self.pens.append(nick)
        
        return self.pens.index(nick) + 1

    def get_nick(self, source):
        if '!' in source:
            return source.split('!',1)[0]
        return source
    
    def on_welcome(self, c, e):
        c.join(self.channel,)
        print "Bot connected"
        self.get_plotter()
    
    def answer(self, c, msg):
        c.privmsg(self.channel, msg)
    
    def write_hpgl(self, c, hpgl):
        thread.start_new_thread(self.write, (c, hpgl))
    
    def write(self, c, hpgl):
        plotter = self.get_plotter()
        
        if plotter <> False:
            self.plotter.write('{0};OI;IN;RO90'.format(hpgl))
            while self.image <> None:
                if self.plotter._serial_port.inWaiting() < 1:
                    if re.match(PLOTTER_SIGNATURE, self.plotter._serial_port.readline()):
                        print 'ready'
                        self.answer(c, MSG_FINISHED_IMAGE)
                        self.image = None
                else:
                    time.sleep(1.0 / 8)
        else:
            self.image = None
            self.answer(c, MSG_FAILED_IMAGE)
            
    
    
    def draw_image(self, c):
        if self.image <> None:
            extension_match = ext_pattern.search(self.image)
            print extension_match
            if (extension_match <> None):
                extension = extension_match.group(1)
                localName = 'drawings/latest'

                imgWidth = '800'
                imgHeight = '800'

                subprocess.call([
                    './hpgl-line-drawing.sh',
                    self.image,
                    localName,
                    extension,
                    imgWidth,
                    imgHeight
                ])

                handle = open('{0}.hpgl'.format(localName), 'r')
                hpgl = ''.join(handle.readlines()).replace('\n', '')

                width = self.margins['right'] - self.margins['left']
                height = self.margins['top'] - self.margins['bottom']
                
                scale = .9
                
                x = randint(2000, width - 5000) * -1
                y = randint(2000, height - 5000) * -1
                
                self.write_hpgl(c, 'SC{0},{1},{2},{3};SP{4};FS1;VS10;PU;{5};PU'.format(x, width + x, y, y + height, self.pen, hpgl))

    def on_pubmsg(self, c, e):
        message = e.arguments[0]
        nick = self.get_nick(e.source)

        found_image = image_pattern.search(message)

        if found_image <> None and self.margins <> None:
            if self.image == None:
                self.image = found_image.group(0)
                self.pen = self.get_pen(nick)
                self.answer(c, MSG_FOUND_IMAGE)
                self.draw_image(c)
            else:
                self.answer(c, MSG_IGNORED_IMAGE)
                
    def has_plotter(self):
        """ Check whether we are connected to a plotter """        
        if self.plotter <> None:
            plotter = what_plotter_in_port(PLOTTER_PORT, self.wait)
            if plotter <> None:
                return True
            else:
                self.plotter = None
                return False
        else:
            return False

    def get_plotter(self):
        if self.has_plotter():
            return self.plotter
        else:
            try:
                self.plotter = _instantiate_plotter(PLOTTER_PORT, PLOTTER_SIGNATURE)
                enlargeMemory(self.plotter)
                self.plotter.write('RO90')
                self.set_margins()
                return self.plotter
            except:
                self.plotter = None
                return False        

    def set_margins(self):
        plottermargins = self.plotter.margins.soft.all_coordinates
        
        self.margins = {
            'left': plottermargins[0],
            'bottom': plottermargins[1],
            'right': plottermargins[2],
            'top': plottermargins[3]
        }
        
        return self.get_margins()
    
    def get_margins(self):
        return self.margins
    
            
if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description='drawbot')
    parser.add_argument('--host', default="irc.freenode.net", help='host')
    parser.add_argument('--port', type=int, default=6667, help='port')
    parser.add_argument('--channel', default="botopera", help='channel to join')
    args = parser.parse_args()
    
    if not args.channel.startswith("#"):
        args.channel = "#"+args.channel
    
    bot = PlotBot(args.channel, 'botopera_roland', args.host, args.port)
    bot.start()