"""
    The HPGL bot litens to private messages, which will be
    forwarded to the plotter, except for when it receives
    it 
"""

from chiplotle.tools.plottertools._instantiate_plotter import _instantiate_plotter
from chiplotle.tools.serialtools import what_plotter_in_port

import irc.bot
from plotterMem import enlargeMemory
import re
import serial
import thread
import time

PLOTTER_SIGNATURE = '0400'
PLOTTER_PORT= '/dev/ttyUSB0'

STATE_WAITING = 'waiting'
STATE_PLOTTING = 'plotting'
STATE_LISTENING = 'listening'

OPEN_STREAM_REGEX = re.compile('^\s*<<(.*)')
CLOSE_STREAM_REGEX = re.compile('(.*)>>\s*$')

MSG_TOO_BUSY = "Sorry, I've no time for it right now."
MSG_LISTENING_TO_SOMEONE_ELSE = "Sorry, I'm currently listening to someone else, try again soon?"
MSG_STARTED_STREAM = "Seems you are about to plot a lot..."
MSG_GOT_PART = "mkay..."
MSG_GOT_IT = "Got it, it'll be ready soon."
MSG_FINISHED = "Hey, great news, your image is ready"

class HPGLBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel
        self.plotter = None
        self.wait = 3
        self.margins = {'left': 0, 'right': 0, 'top': 0, 'bottom': 0}
        self._listen_max = 300
        self._listening_buffer = ''
        self._listening_since = None
        self._listening_to = None
        self.buff = None
        self.state = STATE_WAITING
        self.get_plotter()
    
    def on_privmsg(self, c, e):
        msg = e.arguments[0]
        
        print "Received msg: '{0}'".format(msg)
        
        whom = self.get_nick(e.source)
        
        if msg[-1] == '?':
            if 'doing' in msg <> None or 'state' in msg <> None:
                return self.answer(c, whom, 'I\'m currently {0}'.format(self.state))
            
            margins = self.get_margins()
            plotter = self.get_plotter()
            
            if plotter <> None:
                if 'pagesize'in msg:
                    return self.answer(c, whom, '{0}, {1}, {2}, {3}'.format(margins['left'], margins['bottom'], margins['right'], margins['top']))
                
                if 'left' in msg:
                    return self.answer(c, whom, margins['left'])
                
                if 'right' in msg:
                    return self.answer(c, whom, margins['right'])
                
                if 'top' in msg:
                    return self.answer(c, whom, margins['top'])
                
                if 'bottom' in msg:
                    return self.answer(c, whom, margins['bottom'])
                
        if self.state == STATE_PLOTTING:
            ### DROP 
            return self.answer(c, whom, MSG_TOO_BUSY)
        else:
            if self.listening():
                """
                    We are in listening mode, we'll check
                    to whom we are listening, if it is the
                    same sender, accept message, else drop
                    and answer
                """
                if self.listening_to() <> whom:
                    return self.answer(c, whom, MSG_LISTENING_TO_SOMEONE_ELSE)
                else:
                    hpgl = msg
            else:
                """
                    Check whether the open stream marks are in
                    the message, if so start a stream.
                """
                open_stream_match = OPEN_STREAM_REGEX.search(msg)
                if open_stream_match <> None:
                    self.start_listening(whom)
                    hpgl = open_stream_match.group(1)
                else:
                    hpgl = msg
                
            """
                Check whether the close stream marks are in
                the message, if so close the stream
            """
            close_stream_match = CLOSE_STREAM_REGEX.search(hpgl)
            if close_stream_match <> None:
                self.stop_listening()
                hpgl = self.get_buffer() + close_stream_match.group(1)
                
            if self.state == STATE_LISTENING:
                self.add_to_buffer(hpgl)
                return self.answer(c, whom, MSG_GOT_PART)
            else:
                self.plot(c, whom, hpgl)
                self.clear_buffer()
                return self.answer(c, whom, MSG_GOT_IT)

    """
        Returns whether the plotter is currently listening
    """
    def listening(self):
        if self.state == STATE_LISTENING:
            """ Check whether we are not listening for too long """
            if (time.time() - self._listen_max) < self._listening_since:
                return True
            else:
                self.stop_listening()
        return False

    """
        Returns to whom we are listening
    """
    def listening_to(self):
        return self._listening_to

    def start_listening(self, whom):
        if self.state == STATE_WAITING:
            self.state = STATE_LISTENING
            self.clear_buffer()
            self._listening_since = time.time()
            self._listening_to = whom
    
    def stop_listening(self):
        if self.state == STATE_LISTENING:
            self.state = STATE_WAITING
            self._listening_since = None
            self._listening_to = None
    
    def clear_buffer(self):
        self._listening_buffer = ''
    
    def add_to_buffer(self, hpgl):
        self._listening_buffer += hpgl
    
    def get_buffer(self):
        return self._listening_buffer
    
    def on_welcome(self, c, e):
        c.join(self.channel,)
        print "Bot connected"
    
    def on_pubmsg(self, c, e):
        """ Ignore """
        
    def get_nick(self, source):
        if '!' in source:
            return source.split('!',1)[0]
        return source
    
    def answer(self, c, whom, msg):
        return c.privmsg(whom, msg)
    
    def plot(self, c, whom, hpgl):
        thread.start_new_thread(self.write, (c, whom, hpgl))
    
    def write(self, c, whom, hpgl):
        plotter = self.get_plotter()
        self.state = STATE_PLOTTING
        
        if plotter <> False:
            print hpgl
            self.plotter.write('{0};OI;IN;RO90'.format(hpgl))
            while STATE_PLOTTING == self.state:
                if self.plotter._serial_port.inWaiting() < 1:
                    if re.match(PLOTTER_SIGNATURE, self.plotter._serial_port.readline()):
                        print 'ready'
                        self.answer(c, whom, MSG_FINISHED)
                        self.state = STATE_WAITING
                else:
                    time.sleep(1.0 / 8)
        else:
            self.answer(c, whom, MSG_FINISHED)
            self.state = STATE_WAITING
            
    def has_plotter(self):
        """ Check whether we are connected to a plotter """        
        if self.state == STATE_PLOTTING:
            return True
        else:
            if self.plotter <> None:
                plotter = what_plotter_in_port(PLOTTER_PORT, self.wait)
                if plotter <> None:
                    return True
                else:
                    self.plotter = None
                    return False
            else:
                return False

    def get_plotter(self):
        if self.has_plotter():
            return self.plotter
        else:
            try:
                self.plotter = _instantiate_plotter(PLOTTER_PORT, PLOTTER_SIGNATURE)
                enlargeMemory(self.plotter)
                self.plotter.write('RO90')
                self.set_margins()
                return self.plotter
            except:
                self.plotter = None
                return False
    
    def set_margins(self):
        plottermargins = self.plotter.margins.soft.all_coordinates
        
        self.margins = {
            'left': plottermargins[0],
            'bottom': plottermargins[1],
            'right': plottermargins[2],
            'top': plottermargins[3]
        }
        
        return self.get_margins()
    
    def get_margins(self):
        return self.margins
    
if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description='roland')
    parser.add_argument('--host', default="localhost", help='host')
    parser.add_argument('--port', type=int, default=6667, help='port')
    parser.add_argument('channel', help='channel to join')

    args = parser.parse_args()
    
    if not args.channel.startswith("#"):
        args.channel = "#"+args.channel
    
    bot = HPGLBot(args.channel, 'botopera_roland', args.host, args.port)
    bot.start()