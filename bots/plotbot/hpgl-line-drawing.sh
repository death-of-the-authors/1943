#!/bin/bash
DOWNLOAD=${1}
NAME=${2}
EXT=${3}
MAXWIDTH=${4}
MAXHEIGHT=${5}

# Download imagefile
wget -O ${NAME}.${EXT} ${DOWNLOAD}

# Cut selection
./crop.py ${NAME}.${EXT} ${NAME}.png ${MAXWIDTH} ${MAXHEIGHT}

convert ${NAME}.png ${NAME}.bmp

# Remove tmp files
# rm ${NAME} ${NAME}.png

# First convert to vector and despeckle
potrace -t 20 -o ${NAME}.ps ${NAME}.bmp

# Convert it again into bitmap
convert ${NAME}.ps ${NAME}.jpg

# Use centerline to turn into linedrawing
autotrace -centerline -color-count=2 -background-color=FFFFFF -output-file=${NAME}.svg ${NAME}.jpg

# rm ${NAME}.jpg

# Through inkscape
inkscape -z -P ${NAME}.ps ${NAME}.svg

# HPGL
pstoedit -f plot-hpgl ${NAME}.ps ${NAME}.dirty.hpgl

# Clean HPGL
./hpgl-distiller -i ${NAME}.dirty.hpgl -o ${NAME}.hpgl