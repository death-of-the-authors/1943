import sys
from argparse import ArgumentParser

p = ArgumentParser("map words to routes")
args = p.parse_args()

while True:
	line = sys.stdin.readline()
	if line == '':
		break
	line = line.decode("utf-8")
	if "quiet" in line:
		sys.stdout.write("0\n")
		sys.stdout.flush()
	elif "act one" in line or "act 1" in line:
		sys.stdout.write("1\n")
		sys.stdout.flush()
	elif "act two" in line or "act 2" in line:
		sys.stdout.write("2\n")
		sys.stdout.flush()
	elif "act three" in line or "act 3" in line:
		sys.stdout.write("3\n")
		sys.stdout.flush()

