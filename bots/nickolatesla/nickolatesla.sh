#!/bin/bash
# ensure fifo
if [ ! -e msg.fifo ]; then mkfifo msg.fifo; fi
if [ ! -e delay.fifo ]; then mkfifo delay.fifo; fi
if [ ! -e route_control.fifo ]; then mkfifo route_control.fifo; fi
if [ ! -e route_selector.fifo ]; then mkfifo route_selector.fifo; fi

if [ ! -e route1.fifo ]; then mkfifo route1.fifo; fi
if [ ! -e route2.fifo ]; then mkfifo route2.fifo; fi
if [ ! -e route3.fifo ]; then mkfifo route3.fifo; fi
if [ ! -e collect.fifo ]; then mkfifo collect.fifo; fi

# start the pipeline
function route0 ()
{
cat msg.fifo | \
python ../ircpipebot.py --nickname nickolatesla | \
tee route_selector.fifo delay.fifo | \
python -u ../delay.py --delay 0.5 | \
python -u ../ircfilter.py --block beatrixbotter --block botswaller --block nickolatesla --block rachmanibot --block botopera_roland --message | \
python -u ../router.py --start 1 --control route_control.fifo route1.fifo route2.fifo route3.fifo
}

function routeselector ()
{
cat route_selector.fifo | \
python -u ../routeselector.py | \
cat > route_control.fifo
}

function route1()
{
cat route1.fifo | \
python -u ../whooshbot.py --index ~/bots/tesla.wikipedia tesla.wikipedia.txt | \
cat > collect.fifo
}

function route2()
{
cat route2.fifo | \
python -u ../whooshbot.py --index ~/bots/tesla.works tesla.works.txt | \
cat > collect.fifo
}

function route3()
{
cat route3.fifo | \
python -u ../whooshbot.py --index ~/bots/tesla.remixes tesla.remixes.txt | \
cat > collect.fifo
}

function endroute()
{
cat collect.fifo | \
python -u ../delay.py --receive delay.fifo --delay 0.0 | \
cat > msg.fifo
}

route1 &
route2 &
route3 &
routeselector &
endroute &
route0

# cleanup
rm *.fifo