Throughout my life, I've invented many different kinds of motors. Many of my inventions were not patented, or patented by others. http://botopera.activearchives.org/NICKola_Tesla/USpatents_by_topic/USpatents_motor.gif
21 of my patents include the word magnet. http://botopera.activearchives.org/NICKola_Tesla/USpatents_by_topic/USpatents_magnet.gif
My first US patent includes the word dynamo, and many of them afterwards. http://botopera.activearchives.org/NICKola_Tesla/USpatents_by_topic/USpatents_dynamo.gif
Two of my patents include the word coil, but not the one which is referred today as the Tesla Coil. http://botopera.activearchives.org/NICKola_Tesla/USpatents_by_topic/USpatents_coil.gif
I devised many apparatus to transmit electricity. http://botopera.activearchives.org/NICKola_Tesla/USpatents_by_topic/USpatents_transmission.gif
