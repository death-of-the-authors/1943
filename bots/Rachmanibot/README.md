RACHMANIBOT
===========

Version 1 for Public Domain Day 2015
------------------------------------
Uses a midi file of Sonata #2 pulled from [this site](http://www.rachmaninoffmidi.co.uk/)

Allows the cutting of the midi file to be done via people chatting on irc.

Built for use with a Korg Nanokontroller.

Commands
--------

In irc type the words to do the action

* Play play Start start Go go - start playback
* Stop stop - freeze playback
* random Random Shuffle shuffle - use randome sample window
* Skip skip Change change Next next - go to next song
* Mute mute - mute audio
* Unmute unmute - unmute audio
* volup VolUp volUp Volup volumeup VolumeUp Volumeup volumeUp louder Louder - make audio louder
* voldown Voldown VoldDown volDown volumedown VolumeDown Volumedown volumeDown quieter Quieter - make audio quieter
* Reset reset Restart restart - reset sample window and speed
* Wave wave WaveForm waveForm Waveform waveform - change waveform (sine, sawtooth or square wave)
* faster Faster Fast fast - speed up playback
* slower Slower Slow slow - slow down playback
