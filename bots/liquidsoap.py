import telnetlib
from settings import LIQUIDSOAP_HOST, LIQUIDSOAP_PORT


def request (channel, path_or_url):
    try:
        tn = telnetlib.Telnet(LIQUIDSOAP_HOST, LIQUIDSOAP_PORT)
        tn.write("/botopera(dot)ogg.skip\n")
        tn.write("{0}.push {1}\n".format(channel, path_or_url))
        tn.write("quit\n")
        ret = tn.read_all()
        print "liquidsoap_request", ret
        tn.close()
        return ret
    except Exception as e:
        print e
        return None
