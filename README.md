BOTOPERA
===========

The Death of the Authors, 1943

![](http://www.nova-cinema.org/local/cache-vignettes/L400xH499/botopera.difference-0ea47.png)

Performance "The death of the Authors, 1943 / Le Charme discret de la botoisie/The Discrete Charm of the Botoisie"

---

Fats Waller, Nicolas Tesla, Beatrix Potter, Sergei Rachmaninov and Henri La Fontaine are some of the many artists who have left this world during the second world war. These authors will be reanimated in the form of a "chatbot": a small software program that automatically intervenes in a chat conversation pretending to be human. The bots, the public, the printers and the projected images work together in this interactive performance.

With : BotsWaller, NICKola tesla, Beatrix Plotter, Rachmanibot, henrIRC lafontaine &amp; their plotters.

---

Fats Waller, Nicolas Tesla, Beatrix Potter, Sergei Rachmaninov et Henri La Fontaine font partie des nombreux artistes qui nous ont quitté pendant la seconde guerre mondiale.
Ces auteurs seront réanimés sous la forme du “chatbot”, petit logiciel qui intervient automatiquement dans une conversation chatée en prétendant être un humain. Les bots, le public, les imprimantes et les images projetées collaborent dans une performance interactive.

Avec : BotsWaller, NICKola tesla, Beatrix Plotter, Rachmanibot, henrIRC lafontaine &amp; leurs plotters.

---

Fats Waller, Nicolas Tesla, Beatrix Potter, Sergei Rachmaninov, Henri La Fontaine zijn enkele van de vele kunstenaars die deze wereld hebben verlaten gedurende de Tweede Wereldoorlog. Deze auteurs worden opnieuw tot leven geroepen in de vorm van een “chatbot”: een klein computerprogramma dat tussenkomt in een chat-gesprek en dat zich voordoet als een mens. Bots, publiek, printers en geprojecteerd beeld werken samen in een interactieve performance. Met: BotsWaller, NICKola tesla, Beatrix Plotter, Rachmanibot, henrIRC lafontaine &amp; hun plotters.

Een creatie van Michael Murtaugh, Anne Laforet, Gijs De Heij, Antonio Roberts, An Mertens.

---

[Cinema Nova Event](http://www.nova-cinema.org/programme/2015/146/public-domain-day/?lang=fr#article-14737)
