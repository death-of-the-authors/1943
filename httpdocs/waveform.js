(function () {

function waveform_display (canvas, audioElement) {
    var canvasCtx = canvas.getContext("2d");
    var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
    var analyser = audioCtx.createAnalyser();
    var source = audioCtx.createMediaElementSource(audioElement);
    var gainNode = audioCtx.createGain();
    source.connect(analyser);
    // source.connect(gainNode);
    analyser.connect(gainNode);
    gainNode.connect(audioCtx.destination);

    analyser.fftSize = 2048;
    var bufferLength = analyser.fftSize;
    var dataArray = new Uint8Array(bufferLength);
    canvasCtx.clearRect(0, 0, canvas.width, canvas.height);
    var WIDTH = canvas.width,
        HEIGHT = canvas.height;
    function draw_waveform () {
        drawVisual = requestAnimationFrame(draw_waveform);
        analyser.getByteTimeDomainData(dataArray);
        canvasCtx.fillStyle = 'rgb(0, 0, 0)';
          canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
          canvasCtx.lineWidth = 2;
          canvasCtx.strokeStyle = 'rgb(0, 255, 0)';

          canvasCtx.beginPath();

          var sliceWidth = WIDTH * 1.0 / bufferLength;
          var x = 0;
          for(var i = 0; i < bufferLength; i++) {
       
            var v = dataArray[i] / 128.0;
            var y = v * HEIGHT/2;

            if(i === 0) {
              canvasCtx.moveTo(x, y);
            } else {
              canvasCtx.lineTo(x, y);
            }

            x += sliceWidth;
          }
          canvasCtx.lineTo(canvas.width, canvas.height/2);
          canvasCtx.stroke();
    };
    draw_waveform();
}

window.waveform_display = waveform_display;














})();

