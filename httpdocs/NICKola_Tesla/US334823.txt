

(No Model.)

N. TESLA.

GOMMUTATOR FOR DYNAMO ELEGTRIG MACHINES.

No. 834,823. Patented Jan. 26, 1886.

CW Jwmlz 6% Ma/w (764 5a.

UNITED STATES PATENT OFFICE.

NIKOLA TESLA, OF SMILJAN LIKA, AUSTRIA-HUNGARY, ASSIGNOR TO THE TESLA ELECTRIC LIGHT AND MANUFACTURING COMPANY, OF RAHWAY,

NEIV JERSEY.

COMMUTATOR FOR DYNAMO-ELECTRIC MACHINES.

EPECIPICATION forming part of Letters Patent No. 334,823, dated January 26, 1886.

Application filed May 6, 1885.

To all whom it may concern:

Be it known that I, NIKOLA TESLA, of Smiljan Lika, border country ofAustria-Hungary, have invented an Improvement in Dyname-Electric Machines, of which the following is a specification.

My invention relates to the commutators on dynamo electric machines, especially in machines of great electromoti ve force, adapted IO to are lights; and it consists in a device by means of which the sparking on the commutater is prevented It is known that in machines of great electromotive force-such, for instance, as those 1 5 used for are lights-whenever one commutator bar or plate comes out of contact with the col lectingbrush a spark appears on the commutator. This spark may be due to the break of the complete circuit, or of a shunt of low resistanee formed by the brush between two or more commutator-bars. In the first case the spark is more apparent, as there is at the moment when the circuit is broken a discharge of the magnets through the field-helices, producing a great spark or flash which causes an unsteady current, rapid wear of the commutator bars and brushes, and waste of power. The sparking may be reduced by various devices, such as providing a path for the our- 0 rent at the moment when the commutator segment or bar leaves the brush, by short-circuiting the field-helices, by increasing the number of the connnutator-bars, or by other similar means; but all these devices are ex- 5 pensive or not fully available, and seldom attain the object desired.

My invention enables me to prevent the sparking in a simple manner. For this purpose I employ with the commutator bars 40 and intervening insulating material mica, asbestus paper or other insulating and preferably incombustible material, which I arrange to bear on the surface of the commutator, near to and behind the brush.

My invention will be easily understood by reference to the accompanying drawings.

In the drawings, Figure 1 is a section of a commutator with an asbestus insulating device; and Fig. 2 is a similar View, representing two plates of mica upon the back of the brush.

In Fig. 1, C represents the commutator and Serial No. 164,534. (No model.)

intervening insulating material; B B, the brushes. (1 d are sheets of asbestus paper or other suitable non-conducting material. ff are springs, the pressure of which may be adjusted by means of the screws 9 g.

In Fig. 2 a simple arrangement is shown with two plates of mica or other material. It will be seen that whenever one commutator- 6o segment passes out of contact with the brush the formation of the arc will be prevented by the intervening insulating material coming in contact with the insulating material on the brush.

My invention may be carried out in many ways; and I do not limit myself to any particular device, as my invention consists, broadly, in providing a solid non-conducting body to bear upon the surface of the commutator, by the intervention of which body the sparking is partly or completely prevented.

I prefer to use asbestus paper or cloth impregnated with zinc-oxide, magnesia, zirconia, or other suitable material, as the paper and cloth are soft, and serve at the same time to wipe and polish the commutator; but mica or any other suitable material may be employed, said material being an insulator or a bad coir ductor of electricity.

My invention may be applied to any elec tric apparatus in which sliding contacts are employed.

I claim as my invention 1. The combination, with the commutator- 85 bars and intervening insulating material and brushes in a dynamo electric machine, o'fa solid insulator or bad conductor of electricity arranged to bear upon the surface of the commutator adjacent to the end of the brush, for go the purpose set forth.

2. In an electric apparatus in which sliding contacts wit-h intervening insulating material are employed, the combination, with the contact springs or brushes, of a solid insulator or 5 bad conductor of electricity, as and for the purposes set forth.

Signed by me this 2d day of May, A. D. 1885.

NIKOLA TESLA.

Witnesses:

GEO. T. PINcKNnY, WILLIAM G. Mo'rT.
